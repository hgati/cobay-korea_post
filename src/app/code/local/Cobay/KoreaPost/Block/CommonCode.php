<?php
class Cobay_KoreaPost_Block_CommonCode extends Mage_Adminhtml_Block_Widget_Grid {
	
	protected $isRenderSubTotals;
	
    public function __construct() {
        parent::__construct();
        $this->setId('commonCodeGrid');
        $this->setDefaultSort('cod');
        $this->setDefaultDir('asc');
        //$this->setSaveParametersInSession(true);
		//$this->setSortable(false);
		$this->setPagerVisibility(true);
		$this->setFilterVisibility(true);
		$this->setUseAjax(true);
        //$this->setVarNameFilter('stock_flow_filter');
        $this->setTopMargin(30);
    }

    protected function _prepareCollection(){
		$collection=Mage::getModel('koreapost/code')->getCollection()
		->addFieldToFilter('main_table.p_cod',array('notnull'=>'dumy value'));
		$collection->getSelect()
		->join( 
			array('ccc'=>'cobay_kpost_code'),
			'main_table.p_cod = ccc.cod',
			array(
				'cod'		=>'main_table.cod',
				'cod_nm'	=>'main_table.cod_nm',
				'sort'		=>'main_table.sort',
				'desc'		=>'main_table.desc',
				'p_cod'		=>'ccc.cod',
				'p_cod_nm'	=>'ccc.cod_nm',
				'p_sort'	=>'ccc.sort',
				'p_desc'	=>'ccc.desc',
			),
			null
		);
		//->order('main_table.cod','asc');
		//echo $collection->getSelect(); exit;
		
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    protected function _prepareColumns(){
        $this->addColumn('cod', array(
            'header'    => '코드',
            'width'     => '50px',
            'index'     => 'cod',
            'type'  	=> 'number',
        ));

        $this->addColumn('sort', array(
            'header'    => '정렬',
            'width'     => '10px',
            'index'     => 'sort',
            'type'  	=> 'number',
        	'filter'	=> false
        ));        
        
        $this->addColumn('cod_nm', array(
            'header'	=> '코드명칭',
            'index'		=> 'cod_nm',
        	'filter_index'=>'main_table.cod_nm'
        ));

        $this->addColumn('desc', array(
            'header'	=> '설명',
            'index'		=> 'desc',
        ));        

        /*
        $this->addColumn('p_cod', array(
            'header'    => '상위코드',
            'width'     => '50px',
            'index'     => 'p_cod',
            'type'  	=> 'number',
        	'filter_index'	=> 'ccc.cod'
        ));
        */

        $this->addColumn('p_cod_nm', array(
            'header'		=> '상위코드명칭',
            'index'			=> 'p_cod',
        	'filter_index'	=> 'ccc.cod',
        	'type'			=> 'options',
        ));
                
        
        $this->addExportType('*/*/exportExcel/block/CommonCode', '엑셀');
        $this->addExportType('*/*/exportCsv/block/CommonCode', 'CSV');
                
        return parent::_prepareColumns();
    }


    public function getGridUrl(){
        return $this->getUrl('*/*/CommonCode', array('_current'=>true));
    }

    public function getRowUrl($row){
    	return;
		return	$this->getUrl(
        		'*/order_index/edit', 
        		array(
            		'store'				=>$this->getRequest()->getParam('store'),
            		'id'				=>$row->getId(),
        			'tab'				=>'tab_payments',
        			'ppay_new_class'	=>'open',
        			'backurl'			=>'*,order_payment,index',
        			'backparam'			=>'tab,tab_incomplete'
        		)
        	);
    }

}
