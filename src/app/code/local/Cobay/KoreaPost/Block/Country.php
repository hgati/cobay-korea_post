<?php
class Cobay_KoreaPost_Block_Country extends Mage_Adminhtml_Block_Widget_Grid {
	
	protected $isRenderSubTotals;
	
    public function __construct() {
        parent::__construct();
        $this->setId('countryGrid');
        $this->setDefaultSort('iso3');
        $this->setDefaultDir('asc');
        // $this->setSaveParametersInSession(true);
		//$this->setSortable(false);
		$this->setPagerVisibility(true);
		$this->setFilterVisibility(true);
		$this->setUseAjax(true);
        //$this->setVarNameFilter('stock_flow_filter');
        $this->setTopMargin(30);
    }

    protected function _prepareCollection(){
		$collection=Mage::getModel('koreapost/country')->getCollection();
		$collection->getSelect()
		->join( 
			array('ccc'=>'cobay_country_code'),
			'main_table.iso3 = ccc.iso3',
			array(
				'continent'			=>'ccc.continent',
				'continent_code'	=>'ccc.continent_code'
			),
			null
		);
		
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    protected function _prepareColumns(){
        $this->addColumn('iso3', array(
            'header'	=> $this->__('ISO-3 Code'),
        	'width'		=> '80px',
        	'align'		=> 'center',
            'index'		=> 'iso3',
        ));

        $this->addColumn('country', array(
            'header'	=> $this->__('Country'),
            'index'		=> 'country',
        ));

        $this->addColumn('country_ko', array(
            'header'	=> $this->__('Country (Ko)'),
            'index'		=> 'country_ko',
        ));

        $this->addColumn('rr_full_tracking', array(
            'header'	=> $this->__('RR tracking'),
            'index'		=> 'rr_full_tracking',
        	'width'		=> '70px',
        	'align'		=> 'center',
        ));

        $this->addColumn('continent', array(
            'header'	=> $this->__('Continent'),
            'index'		=> 'continent',
        ));

        $this->addColumn('continent_code', array(
            'header'	=> $this->__('Continent Code'),
            'index'		=> 'continent_code',
        ));        
        
        $this->addExportType('*/*/exportExcel/block/country', '엑셀');
        $this->addExportType('*/*/exportCsv/block/country', 'CSV');
                
        return parent::_prepareColumns();
    }


    public function getGridUrl(){
        return $this->getUrl('*/*/country', array('_current'=>true));
    }

    public function getRowUrl($row){
    	return;
		return	$this->getUrl(
        		'*/order_index/edit', 
        		array(
            		'store'				=>$this->getRequest()->getParam('store'),
            		'id'				=>$row->getId(),
        			'tab'				=>'tab_payments',
        			'ppay_new_class'	=>'open',
        			'backurl'			=>'*,order_payment,index',
        			'backparam'			=>'tab,tab_incomplete'
        		)
        	);
    }

}
