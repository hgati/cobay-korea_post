<?php
class Cobay_KoreaPost_Block_Erd extends Mage_Adminhtml_Block_Template {

	private $rr_fee;
	private $paypal_fee;
	private $price_cutting_unit;
	
    public function __construct() {
		$this->rr_fee = Cobay_KoreaPost_Model_Goods::RR_FEE;
		$this->paypal_fee = (string)Cobay_KoreaPost_Helper_Data::PAYPAL_FEE_RATES;
		$this->paypal_fee = (float)$this->paypal_fee * 0.01;
		$this->price_cutting_unit = (string)Cobay_KoreaPost_Helper_Data::PRICE_CUTTING_UNIT;
		
        return parent::__construct();
    }
    
	protected function _toHtml(){
		$paypal_fee = (string)Cobay_KoreaPost_Helper_Data::PAYPAL_FEE_RATES; // 3.9
		$fee = $paypal_fee * 0.01;
		
		$strHTML = "
			<div style='font-weight:bolder;'>
				<a href='http://www.epost.go.kr' target='_blank'>인터넷 우체국</a>
			</div>
			<div style='font-weight:bolder;'> 
				<a href='http://www.koreapost.go.kr/kpost/main/index.jsp' target='_blank'>우정사업본부</a>
			</div>
			<div style='font-weight:bolder;'>
				<a href='http://wiki.webshopapps.com/extensions/premium-matrix-rate/configuration#TOC-Algorithm-Field' target='_blank'>주문서에 배송방식코드 저장하는 방법</a>
				<font style='font-weight:normal'>(Webshopapps Preminumrate의 알고리즘 필드 사용 :  m=YourShippingCode)</font>
			</div>
			<div style='padding:10px;'>

<script>
function fnChgFormat(_this){
	var obj = document.getElementById('sql-'+_this.value);
	document.getElementById('sql').value = obj.value;
	var obj = document.getElementById('sql2-'+_this.value);
	document.getElementById('sql2').value = obj.value;
}
</script>
<div style='margin-bottom:5px;'>
EMS(비서류), EMS 프리미엄(비서류), 항공소포, SAL항공소포, 항공소형포장물, 항공서장우편<br/>
<b>RR등기비용과 페이팔수수료는 Webshopapps_Premiumrate모듈내에서 자동계산하므로 이 query는 순수 우체국비용만 추출합니다.</b><br/>
<select onchange='fnChgFormat(this)'>
<option value='premiumrate'>for Webshopapps Premiumrate csv format</option>
<option value='matrixrate'>for Webshopapps Matrixrate csv format</option>
</select>
</div>

<textarea id='sql-matrixrate' style='display:none'>
SELECT 
	c.iso3 AS 'Country',
	'*' AS 'Region State',
	'*' AS 'City',
	'*' AS 'Zip_Postal Code',
	'*' AS 'Zip to',
	e.swei AS 'Weight from',
	e.ewei AS 'Weight to',
	0 AS price_from,
	10000000 AS price_to,
	0 AS item_from,
	10000000 AS item_to,
	(
		(
			(CASE WHEN a.gno IN(105) THEN (e.fee + {$this->rr_fee}) ELSE e.fee END)
			+
			((CASE WHEN a.gno IN(105) THEN (e.fee + {$this->rr_fee}) ELSE e.fee END) * {$this->paypal_fee})
		)
		+
		(
			{$this->price_cutting_unit}
			-
			(
				(
					(CASE WHEN a.gno IN(105) THEN (e.fee + {$this->rr_fee}) ELSE e.fee END)
					+
					((CASE WHEN a.gno IN(105) THEN (e.fee + {$this->rr_fee}) ELSE e.fee END) * {$this->paypal_fee})
				)
				%
				{$this->price_cutting_unit}
			)
		)
	) AS shipping_price,
	CONCAT('m=', a.carrier_code) AS `algorithm`,
	a.gnm_en AS 'Delivery Type',
	CASE 
		WHEN a.gno=105 THEN '1' 
		WHEN a.gno=112 THEN '2' 
		WHEN a.gno=102 THEN '3' 
		WHEN a.gno=104 THEN '4' 
		ELSE '*'
	END AS sort_order
FROM
	cobay_kpost_goods a, 
	cobay_kpost_goods_region b, 
	cobay_kpost_goods_region_country c,
	cobay_kpost_goods_wei d,
	cobay_kpost_rate e 
WHERE
	a.gno=b.gno AND b.gno=c.gno AND b.region=c.region AND
	a.gno=d.gno AND d.gno=e.gno AND d.swei=e.swei AND d.ewei=e.ewei AND
	b.gno=e.gno AND b.region=e.region AND a.gno IN (105, 112, 102, 104) AND c.iso3 <> '' 
ORDER BY 
	c.iso3, a.gno, e.swei
</textarea>

<textarea id='sql-premiumrate' style='display:none'>
SELECT 
	c.iso3 AS 'Country',
	'*' AS 'Region State',
	'*' AS 'City',
	'*' AS 'Zip_Postal Code',
	'*' AS 'Zip to',
	e.swei AS 'Weight from',
	e.ewei AS 'Weight to',
	0 AS price_from,
	10000000 AS price_to,
	0 AS item_from,
	10000000 AS item_to,
	e.fee AS shipping_price,
	CONCAT('m=', a.carrier_code) AS `algorithm`,
	a.gnm_en AS 'Delivery Type',
	CASE 
		WHEN a.gno=105 THEN '1' 
		WHEN a.gno=112 THEN '2' 
		WHEN a.gno=102 THEN '3' 
		WHEN a.gno=104 THEN '4' 
		ELSE '*'
	END AS sort_order
FROM
	cobay_kpost_goods a, 
	cobay_kpost_goods_region b, 
	cobay_kpost_goods_region_country c,
	cobay_kpost_goods_wei d,
	cobay_kpost_rate e 
WHERE
	a.gno=b.gno AND b.gno=c.gno AND b.region=c.region AND
	a.gno=d.gno AND d.gno=e.gno AND d.swei=e.swei AND d.ewei=e.ewei AND
	b.gno=e.gno AND b.region=e.region AND a.gno IN (105, 112, 102, 104) AND c.iso3 <> '' 
ORDER BY 
	c.iso3, a.gno, e.swei
</textarea>

<textarea id='sql' cols='80' rows='23'></textarea>
<script>
document.getElementById('sql').value = document.getElementById('sql-premiumrate').value; 
</script>

<textarea id='sql2-matrixrate' style='display:none'>
/* 우편의 종류부터 알아야 하고, 그 다음에 배송수단을 결정해야 하고, 그 다음에 배송수단별로 요금표를 작성해야 한다. */
SELECT 
	k.Country,
	k.Region_State,
	k.City,
	k.Zip_Postal_Code,
	k.Zip_to,
	k.Weight_from,
	k.Weight_to,
	k.Shipping_Price,
	k.Delivery_Type
FROM 
(
	/* EMS(서류,비서류) */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		c.fee AS 'Shipping_Price',
		e.cod_nm AS 'Delivery_Type',
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei		
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.ems_region=c.region AND c.cod=e.cod AND e.cod=102 AND b.ems_region=d.cod 
	UNION ALL 
	/* EMS 프리미엄(서류,비서류) */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		c.fee AS 'Shipping_Price',
		e.cod_nm AS 'Delivery_Type',
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.ems_pri_region=c.region AND c.cod=e.cod AND e.cod=104 AND b.ems_pri_region=d.cod 
	UNION ALL 
	/* 
	항공 소형포장물,
	선편 소형포장물 : 미얀마(MMR), 파푸아뉴기니(PNG)는 csv에서 500g이상 레코드 수동삭제 요망!
	*/
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		c.fee AS 'Shipping_Price',
		e.cod_nm AS 'Delivery_Type',
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod IN (105,106) AND b.air_trade_region=d.cod
	UNION ALL 
	/* 항공 소포 */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		c.fee AS 'Shipping_Price',
		e.cod_nm AS 'Delivery_Type',
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.ems_region=c.region AND c.cod=e.cod AND e.cod = 112 AND b.ems_region=d.cod	
	UNION ALL 
	/* 선편 소포 */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		c.fee AS 'Shipping_Price',
		e.cod_nm AS 'Delivery_Type',
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod=114 AND b.air_trade_region=d.cod
	UNION ALL 
	/* 선편 소포(취급주의및 규격외 소포) */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		c.fee AS 'Shipping_Price',
		e.cod_nm AS 'Delivery_Type',
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod=115 AND b.air_trade_region=d.cod
	UNION ALL 
	/* 항공 서장 */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		c.fee AS 'Shipping_Price',
		e.cod_nm AS 'Delivery_Type',
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod=107 AND b.air_trade_region=d.cod
	UNION ALL 
	/* 항공 서장, 선편 서장 */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		c.fee AS 'Shipping_Price',
		e.cod_nm AS 'Delivery_Type',
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod IN (107,108) AND b.air_trade_region=d.cod	
) k
ORDER BY k.iso3, k.cod, k.swei, k.ewei 
</textarea>

<textarea id='sql2-premiumrate' style='display:none'>
/* 우편의 종류부터 알아야 하고, 그 다음에 배송수단을 결정해야 하고, 그 다음에 배송수단별로 요금표를 작성해야 한다. */
SELECT 
	k.Country,
	k.Region_State,
	k.City,
	k.Zip_Postal_Code,
	k.Zip_to,
	k.Weight_from,
	k.Weight_to,
	k.price_from,
	k.price_to,
	k.item_from,
	k.item_to,
	k.Shipping_Price,
	k.`algorithm`,
	k.Delivery_Type,
	k.sort_order
FROM 
(
	/* EMS(서류,비서류) */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		'*' AS 'price_from',
		'*' AS 'price_to',
		'*' AS 'item_from',
		'*' AS 'item_to',
		c.fee AS 'Shipping_Price',
		'*' AS `algorithm`,
		e.cod_nm AS 'Delivery_Type',
		'*' AS sort_order,
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei		
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.ems_region=c.region AND c.cod=e.cod AND e.cod=102 AND b.ems_region=d.cod 
	UNION ALL 
	/* EMS 프리미엄(서류,비서류) */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		'*' AS 'price_from',
		'*' AS 'price_to',
		'*' AS 'item_from',
		'*' AS 'item_to',		
		c.fee AS 'Shipping_Price',
		'*' AS `algorithm`,		
		e.cod_nm AS 'Delivery_Type',
		'*' AS sort_order,		
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.ems_pri_region=c.region AND c.cod=e.cod AND e.cod=104 AND b.ems_pri_region=d.cod 
	UNION ALL 
	/* 
	항공 소형포장물,
	선편 소형포장물 : 미얀마(MMR), 파푸아뉴기니(PNG)는 csv에서 500g이상 레코드 수동삭제 요망!
	*/
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		'*' AS 'price_from',
		'*' AS 'price_to',
		'*' AS 'item_from',
		'*' AS 'item_to',				
		c.fee AS 'Shipping_Price',
		'*' AS `algorithm`,				
		e.cod_nm AS 'Delivery_Type',
		'*' AS sort_order,				
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod IN (105,106) AND b.air_trade_region=d.cod
	UNION ALL 
	/* 항공 소포 */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		'*' AS 'price_from',
		'*' AS 'price_to',
		'*' AS 'item_from',
		'*' AS 'item_to',						
		c.fee AS 'Shipping_Price',
		'*' AS `algorithm`,						
		e.cod_nm AS 'Delivery_Type',
		'*' AS sort_order,						
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.ems_region=c.region AND c.cod=e.cod AND e.cod = 112 AND b.ems_region=d.cod	
	UNION ALL 
	/* 선편 소포 */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		'*' AS 'price_from',
		'*' AS 'price_to',
		'*' AS 'item_from',
		'*' AS 'item_to',										
		c.fee AS 'Shipping_Price',
		'*' AS `algorithm`,										
		e.cod_nm AS 'Delivery_Type',
		'*' AS sort_order,										
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod=114 AND b.air_trade_region=d.cod
	UNION ALL 
	/* 선편 소포(취급주의및 규격외 소포) */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		'*' AS 'price_from',
		'*' AS 'price_to',
		'*' AS 'item_from',
		'*' AS 'item_to',												
		c.fee AS 'Shipping_Price',
		'*' AS `algorithm`,												
		e.cod_nm AS 'Delivery_Type',
		'*' AS sort_order,												
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod=115 AND b.air_trade_region=d.cod
	UNION ALL 
	/* 항공 서장 */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		'*' AS 'price_from',
		'*' AS 'price_to',
		'*' AS 'item_from',
		'*' AS 'item_to',														
		c.fee AS 'Shipping_Price',
		'*' AS `algorithm`,														
		e.cod_nm AS 'Delivery_Type',
		'*' AS sort_order,														
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod=107 AND b.air_trade_region=d.cod
	UNION ALL 
	/* 항공 서장, 선편 서장 */
	SELECT 
		b.iso3 AS 'Country', 
		'*' AS 'Region_State',
		'*' AS 'City',
		'*' AS 'Zip_Postal_Code',
		'*' AS 'Zip_to',
		c.swei AS 'Weight_from',
		c.ewei AS 'Weight_to',
		'*' AS 'price_from',
		'*' AS 'price_to',
		'*' AS 'item_from',
		'*' AS 'item_to',																
		c.fee AS 'Shipping_Price',
		'*' AS `algorithm`,																
		e.cod_nm AS 'Delivery_Type',
		'*' AS sort_order,																
		d.cod_nm AS 'ETC',
		b.iso3,
		e.cod,
		c.swei,
		c.ewei
	FROM cobay_mage_country a, cobay_kpost_country b, cobay_kpost_rate c, cobay_kpost_code d, cobay_kpost_code e 
	WHERE a.iso3=b.iso3 AND b.air_trade_region=c.region AND c.cod=e.cod AND e.cod IN (107,108) AND b.air_trade_region=d.cod	
) k
ORDER BY k.iso3, k.cod, k.swei, k.ewei 
</textarea>

<textarea id='sql2' cols='91' rows='23' style='color:darkgray;'></textarea>
<script>
document.getElementById('sql2').value = document.getElementById('sql2-premiumrate').value; 
</script>

			</div>			
			<div style='font-weight:bolder; padding-top:10px;'>
				한국우체국 상품데이터모델<br/>
				<img src='".$this->getSkinUrl('images/cobay/koreapost/kpost-erd.jpg')."' style='border:1px darkgray solid;' />
			</div>
		";
		
		return $strHTML;
	}
	
}
