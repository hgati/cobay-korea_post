<?php
class Cobay_KoreaPost_Block_Goods extends Mage_Adminhtml_Block_Widget_Grid {
	
	protected $isRenderSubTotals;
	
    public function __construct() {
        parent::__construct();
        $this->setId('goodsGrid');
        $this->setDefaultSort('gno');
        $this->setDefaultDir('asc');
        //$this->setSaveParametersInSession(true);
		//$this->setSortable(false);
		$this->setPagerVisibility(true);
		$this->setFilterVisibility(true);
		$this->setUseAjax(true);
        //$this->setVarNameFilter('stock_flow_filter');
        $this->setTopMargin(30);
    }

    protected function _prepareCollection(){
		$collection=Mage::getModel('koreapost/goods')->getCollection();
		//echo $collection->getSelect(); exit;
		
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    protected function _prepareColumns(){
        $this->addColumn('gno', array(
            'header'    => $this->__('ID'),
            'width'     => '50px',
            'index'     => 'gno',
        	'type'		=> 'number'
        ));
        
        $this->addColumn('gnm', array(
            'header'	=> $this->__('Goods Name'),
            'index'		=> 'gnm',
        ));

        $this->addColumn('gnm_en', array(
            'header'	=> $this->__('Goods Name (in English)'),
            'index'		=> 'gnm_en',
        ));

        $this->addColumn('carrier_code', array(
            'header'	=> $this->__('Carrier Code'),
            'index'		=> 'carrier_code',
        ));
                
		$this->addColumn('_gno', array(
            'header'    => $this->__('ID (temp)'),
            'width'     => '50px',
            'index'     => '_gno',
			'type'		=> 'number'
        ));
        
        $this->addExportType('*/*/exportExcel/block/goods', '엑셀');
        $this->addExportType('*/*/exportCsv/block/goods', 'CSV');
                
        return parent::_prepareColumns();
    }

    public function getGridUrl(){
        return $this->getUrl('*/*/goods', array('_current'=>true));
    }

    public function getRowUrl($row){
    	return;
		return	$this->getUrl(
        		'*/order_index/edit', 
        		array(
            		'store'				=>$this->getRequest()->getParam('store'),
            		'id'				=>$row->getId(),
        			'tab'				=>'tab_payments',
        			'ppay_new_class'	=>'open',
        			'backurl'			=>'*,order_payment,index',
        			'backparam'			=>'tab,tab_incomplete'
        		)
        	);
    }

}
