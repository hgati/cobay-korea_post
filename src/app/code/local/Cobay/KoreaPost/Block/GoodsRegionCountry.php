<?php
class Cobay_KoreaPost_Block_GoodsRegionCountry extends Mage_Adminhtml_Block_Widget_Grid {
	
	protected $isRenderSubTotals;
	
    public function __construct() {
        parent::__construct();
        $this->setId('goodsRegionCountryGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('asc');
        // $this->setSaveParametersInSession(true);
		//$this->setSortable(false);
		$this->setPagerVisibility(true);
		$this->setFilterVisibility(true);
		$this->setUseAjax(true);
        //$this->setVarNameFilter('stock_flow_filter');
        $this->setTopMargin(30);
    }

    protected function _prepareCollection(){
		$collection=Mage::getModel('koreapost/GoodsRegionCountry')->getCollection();
		$collection->getSelect()
			->join( 
				array('g'=>'cobay_kpost_goods'),
				'main_table.gno = g.gno',
				array(
					'id'	=>'main_table.id',
					'gnm'	=>'g.gnm',
					'gno'	=>'g.gno'
				),
				null
			)
			->join(
				array('cb'=>'cobay_kpost_code'),
				'main_table.region = cb.cod',
				array(
					'region_nm'		=>'cb.cod_nm',
					'region'		=>'main_table.region'
				),
				null			
			)
			->join(
				array('ckc'=>'cobay_kpost_country'),
				'main_table.iso3 = ckc.iso3',
				array(
					'iso3'			=>'ckc.iso3',
					'country'		=>'ckc.country',
					'country_ko'	=>'ckc.country_ko'
				),
				null			
			);			
		//echo $collection->getSelect(); exit;
		
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    protected function _prepareColumns(){
    	
        $this->addColumn('id', array(
            'header'    => $this->__('ID'),
            'width'     => '50px',
            'index'     => 'id',
            'type'  	=> 'number',
        ));
        
        $this->addColumn('gnm', array(
            'header'	=> $this->__('Goods Name'),
            'index'		=> 'gnm',
        ));

        $this->addColumn('region_nm', array(
            'header'	=> $this->__('Region Name'),
            'index'		=> 'region_nm',
        	'filter_index'=> 'cb.cod_nm'
        ));

        $this->addColumn('country', array(
            'header'	=> $this->__('Country'),
            'index'		=> 'country',
        ));

        $this->addColumn('country_ko', array(
            'header'	=> $this->__('Country (Ko)'),
            'index'		=> 'country_ko',
        ));        
        
        $this->addExportType('*/*/exportExcel/block/GoodsRegionCountry', '엑셀');
        $this->addExportType('*/*/exportCsv/block/GoodsRegionCountry', 'CSV');
                
        return parent::_prepareColumns();
    }


    public function getGridUrl(){
        return $this->getUrl('*/*/GoodsRegionCountry', array('_current'=>true));
    }

    public function getRowUrl($row){
    	return;
		return	$this->getUrl(
        		'*/order_index/edit', 
        		array(
            		'store'				=>$this->getRequest()->getParam('store'),
            		'id'				=>$row->getId(),
        			'tab'				=>'tab_payments',
        			'ppay_new_class'	=>'open',
        			'backurl'			=>'*,order_payment,index',
        			'backparam'			=>'tab,tab_incomplete'
        		)
        	);
    }

}
