<?php
class Cobay_KoreaPost_Block_GoodsWeight extends Mage_Adminhtml_Block_Widget_Grid {
	
	protected $isRenderSubTotals;
	
    public function __construct() {
        parent::__construct();
        $this->setId('goodsWeightGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('asc');
        // $this->setSaveParametersInSession(true);
		//$this->setSortable(false);
		$this->setPagerVisibility(true);
		$this->setFilterVisibility(true);
		$this->setUseAjax(true);
        //$this->setVarNameFilter('stock_flow_filter');
        $this->setTopMargin(30);
    }

    protected function _prepareCollection(){
		$collection=Mage::getModel('koreapost/GoodsWeight')->getCollection();
		$collection->getSelect()
			->join( 
				array('g'=>'cobay_kpost_goods'),
				'main_table.gno = g.gno',
				array(
					'id'	=>'main_table.id',
					'swei'	=>'main_table.swei',
					'ewei'	=>'main_table.ewei',
					'gnm'	=>'g.gnm',
					'gno'	=>'g.gno'
				),
				null
			);
		//echo $collection->getSelect(); exit;
		
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    protected function _prepareColumns(){
    	
        $this->addColumn('id', array(
            'header'    => $this->__('ID'),
            'width'     => '50px',
            'index'     => 'id',
            'type'  	=> 'number',
        ));
        
        $this->addColumn('gnm', array(
            'header'	=> $this->__('Goods Name'),
            'index'		=> 'gnm',
        ));

        $this->addColumn('swei', array(
            'header'	=> $this->__('From Weight (Kg)'),
        	'width'		=> '100px',
            'index'		=> 'swei',
        	'align'		=> 'center',
        	//'type'  	=> 'number',
        ));

        $this->addColumn('ewei', array(
            'header'	=> $this->__('To Weight (Kg)'),
        	'width'		=> '100px',
            'index'		=> 'ewei',
        	'align'		=> 'center',
        	//'type'  	=> 'number',
        ));        
        
        $this->addExportType('*/*/exportExcel/block/GoodsWeight', '엑셀');
        $this->addExportType('*/*/exportCsv/block/GoodsWeight', 'CSV');
                
        return parent::_prepareColumns();
    }


    public function getGridUrl(){
        return $this->getUrl('*/*/GoodsWeight', array('_current'=>true));
    }

    public function getRowUrl($row){
    	return;
		return	$this->getUrl(
        		'*/order_index/edit', 
        		array(
            		'store'				=>$this->getRequest()->getParam('store'),
            		'id'				=>$row->getId(),
        			'tab'				=>'tab_payments',
        			'ppay_new_class'	=>'open',
        			'backurl'			=>'*,order_payment,index',
        			'backparam'			=>'tab,tab_incomplete'
        		)
        	);
    }

}
