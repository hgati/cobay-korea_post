<?php
class Cobay_KoreaPost_Block_Header extends Mage_Adminhtml_Block_Widget_Container {

	public function __construct(){
		parent::__construct();
        $this->setTitle($this->__('Manage Korea-Post DB'));
        
        $this->addButton('refresh_for_webshop_btn', array(
            'label'     => $this->__('Refresh for WebshopMatrix'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/refreshWebshopMatrix') . '\')',
            'class'     => 'save',
        ));

        $this->addButton('truncate_for_webshop_btn', array(
            'label'     => $this->__('Truncate for WebshopMatrix'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/truncateWebshopMatrix') . '\')',
            'class'     => 'delete',
        ));        
                
        $this->addButton('erd_btn', array(
            'label'     => $this->__('Korea-Post Data Model'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/showDataModel') . '\')',
            'class'     => 'add',
        ));
    }
    
	/* 이거 없으면 작동안함..템플릿파일에서..*/
    public function isSingleStoreMode() {
        if (!Mage::app()->isSingleStoreMode()) {
               return false;
        }
        return true;
    }

}