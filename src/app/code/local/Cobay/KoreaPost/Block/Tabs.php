<?php
class Cobay_KoreaPost_Block_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

	public function __construct(){
        parent::__construct();
        $this->setId('koreapost_tabs');
        //$this->setDestElementId('main-container');
        $this->setDestElementId('koreapost_tabs');
        //$this->setTitle(Mage::helper('customer')->__('Order Preparation'));
        $this->setTemplate('widget/tabshoriz.phtml');
    }

    protected function _beforeToHtml(){
    	
        $this->addTab('tab_goods', array(
            'label'     => $this->__('goods'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/goods', 
        						array('_current' => true)
        					),
        ));

        $this->addTab('tab_goods_region', array(
            'label'     => $this->__('goods region'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/GoodsRegion', 
        						array('_current' => true)
        					),
        ));

        $this->addTab('tab_goods_region_country', array(
            'label'     => $this->__('goods region country'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/GoodsRegionCountry', 
        						array('_current' => true)
        					),
        ));

        $this->addTab('tab_goods_weight', array(
            'label'     => $this->__('goods weight'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/GoodsWeight', 
        						array('_current' => true)
        					),
        ));        

        $this->addTab('tab_rate', array(
            'label'     => $this->__('rate'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/rate', 
        						array('_current' => true)
        					),
        ));                

        $this->addTab('tab_mage_country', array(
            'label'     => $this->__('mage country'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/MageCountry', 
        						array('_current' => true)
        					),
        ));        

        $this->addTab('tab_country', array(
            'label'     => $this->__('country'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/country', 
        						array('_current' => true)
        					),
        ));        

        $this->addTab('tab_common_code', array(
            'label'     => $this->__('common code'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/CommonCode', 
        						array('_current' => true)
        					),
        ));

        $this->addTab('tab_webshop_matrix', array(
            'label'     => $this->__('Webshop Matrix'),
			'class'     => 'ajax',
			'url'       => $this->getUrl(
								'*/*/WebshopMatrix', 
        						array('_current' => true)
        					),
			'active'	=> true
        ));        

        /*
        $this->addTab('custom_aka_freely_assigned_tab3_id_name', array(
            'label'     => Mage::helper('coffefreakhelper1')->__('Custom tab3 here<br />(usses class block)'),
            'title'     => Mage::helper('coffefreakhelper1')->__('My custom tab3 title here'),
            'content'   => $this->getLayout()->createBlock("coffefreakblock2/SampleBlockForTabAreaShowoff")->toHtml(),
            'active'    => false
        ));
        */    
                
        $this->_updateActiveTab();
        return parent::_beforeToHtml();
    }

 	protected function _updateActiveTab() {
		$tabId = $this->getRequest()->getParam('tab');
		if( $tabId ) {
			$tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
			if($tabId) {
				$this->setActiveTab($tabId);
			}
		}
	}
	    
}
