<?php
class Cobay_KoreaPost_Block_WebshopMatrix extends Mage_Adminhtml_Block_Widget_Grid {
	
	protected $isRenderSubTotals;
	private $rr_fee;
	
    public function __construct() {
        parent::__construct();
        $this->setId('webshopMatrixGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('asc');
        //$this->setSaveParametersInSession(true);
		//$this->setSortable(false);
		$this->setPagerVisibility(true);
		$this->setFilterVisibility(true);
		$this->setUseAjax(true);
        //$this->setVarNameFilter('stock_flow_filter');
        $this->setTopMargin(30);

		$this->rr_fee = Cobay_KoreaPost_Helper_Data::RR_FEE;
    }

    protected function _getStore(){
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
        
    protected function _prepareCollection(){
		$collection=Mage::getModel('koreapost/WebshopMatrix')->getCollection();
		$collection->getSelect()
		->joinLeft( 
			array('ckc'=>'cobay_kpost_country'),
			'main_table.iso3 = ckc.iso3',
			array(
				'country'	=>'ckc.country',
			),
			null
		)
		->joinLeft( 
			array('ckg'=>'cobay_kpost_goods'),
			'main_table.gno = ckg.gno',
			array(
				'gno'		=>'ckg.gno',
				'gnm'		=>'ckg.gnm',
				'gnm_en'	=>'ckg.gnm_en',
			),
			null
		)
		->joinLeft( 
			array('ckgd'=>'cobay_kpost_goods_delivery'),
			'ckc.iso3 = ckgd.iso3 and ckg.gno=ckgd.gno',
			array(
				's_deli_time'	=>'ckgd.s_deli_time',
				'e_deli_time'	=>'ckgd.e_deli_time',
			),
			null
		);		
		//echo $collection->getSelect(); exit;
		
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    protected function _prepareColumns(){
		$paypal_fee = (string)Cobay_KoreaPost_Helper_Data::PAYPAL_FEE_RATES;
		$price_cutting_unit = (string)Cobay_KoreaPost_Helper_Data::PRICE_CUTTING_UNIT;
		
    	$store = $this->_getStore();
    	
        $this->addColumn('id', array(
            'header'    => $this->__('ID'),
            'width'     => '40px',
        	'filter'	=> false,
            'index'     => 'id',
            'type'  	=> 'number',
        ));

        $this->addColumn('iso3', array(
            'header'	=> $this->__('ISO-3 Code'),
        	'width'		=> '40px',
        	'align'		=> 'center',
            'index'		=> 'iso3',
        	'filter_index'=>'main_table.iso3'
        ));
        
        $this->addColumn('country', array(
            'header'	=> $this->__('Country'),
            'index'		=> 'country',
        	'inline_css'=> 'koreapost-input',
        	'type'		=> 'input'
        ));

        /*
        $this->addColumn('region_state', array(
            'header'	=> $this->__('Region State'),
            'index'		=> 'region_state',
        ));        

        $this->addColumn('city', array(
            'header'	=> $this->__('City'),
            'index'		=> 'city',
        ));                
        
        $this->addColumn('zip_postal_code', array(
            'header'	=> $this->__('Zip_Postal Code'),
            'index'		=> 'zip_postal_code',
        ));        

        $this->addColumn('zip_to', array(
            'header'	=> $this->__('Zip_to'),
            'index'		=> 'zip_to',
        ));
        */                

        $this->addColumn('weight_from', array(
            'header'	=> $this->__('From Weight (Kg)'),
        	'width'		=> '80px',
            'index'		=> 'weight_from',
        	'align'		=> 'center',
        	//'type'		=> 'number',
        ));

        $this->addColumn('weight_to', array(
            'header'	=> $this->__('To Weight (Kg)'), 
        	'width'		=> '80px',
            'index'		=> 'weight_to',
        	'align'		=> 'center',
        	//'type'		=> 'number',
        ));                        

        $this->addColumn('s_deli_time', array(
            'header'	=> $this->__('deli-time from'), 
        	'width'		=> '80px',
            'index'		=> 's_deli_time',
        	'align'		=> 'center',
        	//'type'		=> 'number'
        ));        

		$this->addColumn('e_deli_time', array(
            'header'	=> $this->__('deli-time to'), 
        	'width'		=> '80px',
            'index'		=> 'e_deli_time',
			'align'		=> 'center',
        	//'type'		=> 'number'
        ));
                
        /*
        $this->addColumn('weight_from2', array(
            'header'	=> $this->__('Weight from'),
        	'width'		=> '60px',
        	'filter'	=> false,
        	'type'		=> 'number',
        	'align'		=> 'right',
            'index'		=> 'weight_from',
        ));                

        $this->addColumn('weight_to2', array(
            'header'	=> $this->__('Weight to'),
        	'width'		=> '60px',
        	'filter'	=> false,
        	'type'		=> 'number',
        	'align'		=> 'right',
            'index'		=> 'weight_to',
        ));
        */                        
                
        $this->addColumn('shipping_price', array(
            'header'	=> $this->__('Shipping Price'),
        	'width'		=> '100px',
            'index'		=> 'shipping_price',
        	'type'		=> 'price',
			'currency_code' => $store->getBaseCurrency()->getCode(),
        ));

        $this->addColumn('gno', array(
            'header'		=> $this->__('Goods Name'),
        	'width'			=> '140px',
            'index'			=> 'gno',
        	'filter_index'	=> 'main_table.gno',
            'type'			=> 'options',
            'options'		=> Mage::getModel('koreapost/goods')->getGoodsOptions(),
        ));

        $this->addColumn('gnm_en', array(
            'header'	=> $this->__('Delivery Type'),
            'index'		=> 'gnm_en',
        ));        

                
        $this->addExportType('*/*/exportExcel/block/WebshopMatrix', '엑셀');
        $this->addExportType('*/*/exportCsv/block/WebshopMatrix', 'CSV');
                
        return parent::_prepareColumns();
    }


    public function getGridUrl(){
        return $this->getUrl('*/*/WebshopMatrix', array('_current'=>true));
    }

    public function getRowUrl($row){
    	return;
		return	$this->getUrl(
        		'*/order_index/edit', 
        		array(
            		'store'				=>$this->getRequest()->getParam('store'),
            		'id'				=>$row->getId(),
        			'tab'				=>'tab_payments',
        			'ppay_new_class'	=>'open',
        			'backurl'			=>'*,order_payment,index',
        			'backparam'			=>'tab,tab_incomplete'
        		)
        	);
    }
    
}
