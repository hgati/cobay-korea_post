<?php
class Cobay_KoreaPost_Helper_Data extends Mage_Core_Helper_Data {

	const PAYPAL_FEE_RATES = 3.9;
	const PAYPAL_FEE_FIXED = 0.30;
	const RR_FEE = 2800;
	const PRICE_CUTTING_UNIT = 100;
 
}