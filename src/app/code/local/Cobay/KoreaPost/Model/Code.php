<?php 
class Cobay_KoreaPost_Model_Code extends Mage_Core_Model_Abstract {

	
	protected function _construct(){
		parent::_construct();
		$this->_init('koreapost/code');
	}


	public function getCodeGubun(){

		$retour = array();
		
		$collection = Mage::getModel('koreapost/code')->getCollection()
		->addFieldToFilter('p_cod', array('null'=>'dumy'));
		foreach ($collection as $code){
			$retour[$code->getcod()] = $code->getcod_nm(); 
		}

		return $retour;	
	}
}