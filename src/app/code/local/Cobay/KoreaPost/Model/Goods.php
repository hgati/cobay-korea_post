<?PHP
class Cobay_KoreaPost_Model_Goods extends Mage_Core_Model_Abstract {

	const RR_FEE = 2800;
	
	protected function _construct(){
		parent::_construct();
		$this->_init('koreapost/goods');
	}

	public function getGoodsOptions(){
		$retour = array();
		
		$collection = Mage::getModel('koreapost/goods')->getCollection();
		foreach ($collection as $goods){
			$retour[$goods->getGno()] = $goods->getGnm(); 
		}

		return $retour;	
    }
}
?>