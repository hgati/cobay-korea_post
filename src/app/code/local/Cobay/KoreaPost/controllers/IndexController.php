<?php 
class Cobay_KoreaPost_IndexController extends Mage_Adminhtml_Controller_Action {

	public function indexAction() {
		$this->_title($this->__('System'))->_title($this->__('Tools'))->_title($this->__('Manage Korea-Post'));
        $this->loadLayout();
        $this->_setActiveMenu('system/tools');
        
		// 상단 헤더블럭
        $this->_addContent(
        	$this->getLayout()->createBlock(
        		'koreapost/header',
        		'koreapost_header'
        	)
        );
        
        // 수평탭 블럭
        $this->_addContent(
        	$this->getLayout()->createBlock(
        		'koreapost/tabs',
        		'koreapost_tabs'
        	)
        );
        
        $this->renderLayout();
		
    }

	public function showDataModelAction(){
		$this->_title($this->__('System'))->_title($this->__('Tools'))->_title($this->__('Manage Korea-Post'));
        $this->loadLayout();
        $this->_setActiveMenu('system/tools');
        $this->_addContent(
        	$this->getLayout()->createBlock('koreapost/erd', 'erd')
        );
        
        $this->renderLayout();
	}

    /*  
     * 우체국 DB를 가공하여 webshopapps matrixrate DB용으로  만들기
     * 메모리 테이블은 기본적으로 16M로 제한적이다. /etc/my.cnf에서 max_heap_table_size = 50M정도로 늘려야 한다.
     * 아니면, 저장공간부족으로 인서트가 안된다.
     * 혹시, 한번에 쿼리리턴값이 많으면, max_allowed_packet = 200M 도 고려하라~~
     */    
	public function refreshWebshopMatrixAction(){
		$paypal_fee = (string)Cobay_KoreaPost_Helper_Data::PAYPAL_FEE_RATES;
		$paypal_fee = (float)$paypal_fee * 0.01;
		$rr_fee		= Cobay_KoreaPost_Helper_Data::RR_FEE;
		
		$price_cutting_unit = (string)Cobay_KoreaPost_Helper_Data::PRICE_CUTTING_UNIT;
		// cutting logic : price+(100-(price%100))
				
		$w = Mage::getSingleton('core/resource')->getConnection('core_write');

		$sql = "TRUNCATE `cobay_webshop_matrix`";
		$w->query($sql, array());
		
		$sql = "ALTER TABLE `cobay_webshop_matrix` AUTO_INCREMENT=1";
		$w->query($sql, array());
		
		$sql = "
			INSERT INTO cobay_webshop_matrix(
				iso3, region_state, city, zip_postal_code, zip_to, weight_from, weight_to, shipping_price, gno
			)
			SELECT 
				c.iso3 AS country,	
				'*' AS region_state,
				'*' AS city,
				'*' AS zip_postal_code,
				'*' AS zip_to,
				e.swei AS weight_from,
				e.ewei AS weight_to,
				(
					(
						(case when a.gno IN (107,105) then (e.fee + $rr_fee) else e.fee end)
						+
						((case when a.gno IN (107,105) then (e.fee + $rr_fee) else e.fee end) * $paypal_fee)
					)
					+
					(
						$price_cutting_unit
						-
						(
							(
								(case when a.gno IN (107,105) then (e.fee + $rr_fee) else e.fee end)
								+
								((case when a.gno IN (107,105) then (e.fee + $rr_fee) else e.fee end) * $paypal_fee)
							)
							%
							$price_cutting_unit
						)
					)
				) as shipping_price,
				a.gno AS gno
			FROM
				cobay_kpost_goods a, 
				cobay_kpost_goods_region b, 
				cobay_kpost_goods_region_country c,
				cobay_kpost_goods_wei d,
				cobay_kpost_rate e 
			WHERE
				a.gno=b.gno AND b.gno=c.gno AND b.region=c.region AND
				a.gno=d.gno AND d.gno=e.gno AND d.swei=e.swei AND d.ewei=e.ewei AND
				b.gno=e.gno AND b.region=e.region
			ORDER BY 
				c.iso3, a.gno, e.swei 
		";
		$w->query($sql, array()); 
		
		$this->_getSession()->addSuccess($this->__('Data for webshopapps matrixrate has been just generated.'));
		$this->getResponse()->setRedirect($this->getUrl('*/*/', array('tab'=>'tab_webshop_matrix')));
	}

	public function truncateWebshopMatrixAction(){
		$w = Mage::getSingleton('core/resource')->getConnection('core_write');
		
		$sql = "TRUNCATE `cobay_webshop_matrix`";
		$w->query($sql, array());
		
		$sql = "ALTER TABLE `cobay_webshop_matrix` AUTO_INCREMENT=1";
		$w->query($sql, array());

		$this->_getSession()->addSuccess($this->__('Data for webshopapps matrixrate has been truncated.'));
		$this->getResponse()->setRedirect($this->getUrl('*/*/', array('tab'=>'tab_webshop_matrix')));		
	}
		
	public function GoodsAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/goods', 'koreapost_goods')->toHtml()
		);
	}

	public function GoodsRegionAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/GoodsRegion', 'koreapost_goods_region')->toHtml()
		);
	}

	public function GoodsRegionCountryAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/GoodsRegionCountry', 'koreapost_goods_region_country')->toHtml()
		);
	}

	public function GoodsWeightAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/GoodsWeight', 'koreapost_goods_weight')->toHtml()
		);
	}	

	public function RateAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/rate', 'koreapost_rate')->toHtml()
		);
	}

	public function MageCountryAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/MageCountry', 'mage_country')->toHtml()
		);
	}	

	public function CountryAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/country', 'country')->toHtml()
		);
	}

	public function CommonCodeAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/CommonCode', 'common_code')->toHtml()
		);
	}

	public function WebshopMatrixAction(){
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('koreapost/WebshopMatrix', 'webshop_matrix')->toHtml()
		);
	}	
	
    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction() {
    	$blockname = $this->getRequest()->getParam('block');
    	
        $fileName   = "export-$blockname.xml";
        $grid       = $this->getLayout()->createBlock("koreapost/$blockname");
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction() {
    	$blockname = $this->getRequest()->getParam('block');
    	
        $fileName   = "export-$blockname.xml";
        $grid       = $this->getLayout()->createBlock("koreapost/$blockname");
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }    

}
