<?php 
$installer = $this;
$installer->startSetup();
$installer->run("
	ALTER TABLE `{$installer->getTable('koreapost/WebshopMatrix')}` DROP COLUMN delivery_type;

	ALTER TABLE `{$installer->getTable('koreapost/WebshopMatrix')}` 
		ADD COLUMN `gno` INT COMMENT '우체국상품번호' AFTER `shipping_price`;
");
$installer->endSetup();
