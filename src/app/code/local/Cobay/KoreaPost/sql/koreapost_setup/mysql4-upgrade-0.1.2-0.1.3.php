<?php 
$installer = $this;
$installer->startSetup();
/* 요금인상건 : EMS(서류, 비서류)에 306 (특정6지역) 추가됨 */
$installer->run("
	insert  into `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`,`region`) 
	values(101, 306),(102, 306);
");

$installer->endSetup();