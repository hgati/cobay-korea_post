<?php 
$installer = $this;
$installer->startSetup();
/* 요금인상건 : EMS(서류, 비서류)에 306 (특정6지역: 영국GBR, 프랑스FRA, 스페인 ESP) 추가됨 */
$installer->run("
	INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(`gno`,`region`,`iso3`) 
	VALUES
	(101, 306, 'GBR'),
	(101, 306, 'FRA'),
	(101, 306, 'ESP'),
	(102, 306, 'GBR'),
	(102, 306, 'FRA'),
	(102, 306, 'ESP');	
");

$installer->endSetup();