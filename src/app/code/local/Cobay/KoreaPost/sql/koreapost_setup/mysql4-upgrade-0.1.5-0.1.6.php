<?php 
$installer = $this;
$installer->startSetup();

/* 요금인상건 : 항공소포(112)에 306 (특정6지역) 추가됨 */
$installer->run("
	INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`,`region`) VALUES(112, 306);
");

/* 요금인상건 : 이전버전에서 빼먹은거 업데이트  */
$installer->run("
	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=101 AND `region`=203 AND `iso3`='GBR';
	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=102 AND `region`=203 AND `iso3`='GBR';
	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=112 AND `region`=203 AND `iso3`='GBR';
	
	INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(`gno`,`region`,`iso3`) VALUES(112, 306, 'GBR');

	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=101 AND `region`=203 AND `iso3`='FRA';
	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=102 AND `region`=203 AND `iso3`='FRA';
	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=112 AND `region`=203 AND `iso3`='FRA';

	INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(`gno`,`region`,`iso3`) VALUES(112, 306, 'FRA');
	
	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=101 AND `region`=203 AND `iso3`='ESP';
	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=102 AND `region`=203 AND `iso3`='ESP';
	DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE `gno`=112 AND `region`=203 AND `iso3`='ESP';
	
	INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(`gno`,`region`,`iso3`) VALUES(112, 306, 'ESP');
	
");


$installer->endSetup();