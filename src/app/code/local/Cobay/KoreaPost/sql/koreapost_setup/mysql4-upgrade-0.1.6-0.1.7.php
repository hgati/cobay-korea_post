<?php 
$installer = $this;
$installer->startSetup();

/* 배송방식코드 필드추가 : 주문서에 저장하기 위한용도 */
$installer->run("
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` 
		ADD COLUMN `carrier_code` CHAR(4) NOT NULL DEFAULT '' COMMENT '배송방식코드' AFTER `gnm_en`;

	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='EMSS' WHERE `gno`=101;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='EMSG' WHERE `gno`=102;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='EMPS' WHERE `gno`=103;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='EMPG' WHERE `gno`=104;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='AIRP' WHERE `gno`=105;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='SEAP' WHERE `gno`=106;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='AIRC' WHERE `gno`=107;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='SEAC' WHERE `gno`=108;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='AIRY' WHERE `gno`=109;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='SEAY' WHERE `gno`=110;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='AIRG' WHERE `gno`=111;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='AIRS' WHERE `gno`=112;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='SALS' WHERE `gno`=113;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='SEAS' WHERE `gno`=114;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `carrier_code`='ETCS' WHERE `gno`=115;
	
	CREATE UNIQUE INDEX `idx_cobay_kpost_goods_carrier_code` ON `{$installer->getTable('koreapost/goods')}` ( carrier_code );
		
");

$installer->endSetup();