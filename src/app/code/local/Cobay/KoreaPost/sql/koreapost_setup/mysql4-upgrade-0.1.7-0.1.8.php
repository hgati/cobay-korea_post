<?php 
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` ADD COLUMN `gnm_en_sub` VARCHAR(30) COMMENT '상품명 뒤에 붙는 괄호서브타이틀' AFTER `gnm_en`;
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` ADD COLUMN `s_deli_time` TINYINT COMMENT '예상소요일(~부터)' AFTER `gnm_en_sub`;
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` ADD COLUMN `e_deli_time` TINYINT COMMENT '예상소요일(~까지)' AFTER `s_deli_time`;
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` ADD COLUMN `cost_ment` VARCHAR(30) COMMENT '비용멘트' AFTER `e_deli_time`;
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` ADD COLUMN `max_weight` TINYINT COMMENT '최대중량제한(kg)' AFTER `cost_ment`;

	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en_sub`='International Express Mail' WHERE `gno`=101;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en_sub`='International Express Mail' WHERE `gno`=102;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en_sub`='TNT' WHERE `gno`=103;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en_sub`='TNT' WHERE `gno`=104;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en`='Small Packet',	`gnm_en_sub`='Air' WHERE `gno`=105;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en`='Small Packet',	`gnm_en_sub`='Sea' WHERE `gno`=106;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en`='Parcel Post',	`gnm_en_sub`='Air' WHERE `gno`=112;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en`='SAL Parcel Post',	`gnm_en_sub`='Air' WHERE `gno`=113;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `gnm_en`='Parcel Post',	`gnm_en_sub`='Sea' WHERE `gno`=114;
	
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `s_deli_time`=3 WHERE `gno`=101;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `e_deli_time`=5 WHERE `gno`=101;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `s_deli_time`=3 WHERE `gno`=102;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `e_deli_time`=5 WHERE `gno`=102;

	UPDATE `{$installer->getTable('koreapost/goods')}` SET `s_deli_time`=2 WHERE `gno`=103;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `e_deli_time`=4 WHERE `gno`=103;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `s_deli_time`=2 WHERE `gno`=104;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `e_deli_time`=4 WHERE `gno`=104;
	
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `s_deli_time`=6 WHERE `gno`=112;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `e_deli_time`=12 WHERE `gno`=112;
	
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `s_deli_time`=14 WHERE `gno`=105;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `s_deli_time`=20 WHERE `gno`=105;
	
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `cost_ment`='Expensive' WHERE `gno`=101;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `cost_ment`='Expensive' WHERE `gno`=102;

	UPDATE `{$installer->getTable('koreapost/goods')}` SET `cost_ment`='Most Expensive' WHERE `gno`=103;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `cost_ment`='Most Expensive' WHERE `gno`=104;

	UPDATE `{$installer->getTable('koreapost/goods')}` SET `cost_ment`='Cheap' WHERE `gno`=112;
	
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `cost_ment`='Cheapest' WHERE `gno`=105;
	
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=2 WHERE `gno`=101;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=30 WHERE `gno`=102;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=2 WHERE `gno`=103;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=50 WHERE `gno`=104;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=20 WHERE `gno`=112;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=20 WHERE `gno`=113;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=20 WHERE `gno`=114;
	
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=2 WHERE `gno`=105;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_weight`=2 WHERE `gno`=106;
	
");

$installer->endSetup();