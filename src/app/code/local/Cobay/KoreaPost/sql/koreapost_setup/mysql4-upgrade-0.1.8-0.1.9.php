<?php 
$installer = $this;
$installer->startSetup();

/* 잘못 입력된 데이터 정정 */
$installer->run("
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `s_deli_time`=14 WHERE `gno`=105;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `e_deli_time`=20 WHERE `gno`=105;
");

$installer->endSetup();