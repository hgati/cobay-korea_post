<?php 
$installer = $this;
$installer->startSetup();

$installer->run("
	INSERT INTO `{$installer->getTable('koreapost/goods')}`(`gno`, `gnm`, `gnm_en`, `gnm_en_sub`, `carrier_code`) 
	VALUES(116, '국제등기', 'Insternational Registration', 'Optional', 'IREG'); 
 
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` ADD COLUMN `tracking_code_format` VARCHAR(30) COMMENT '트래킹코드포맷' AFTER `max_weight`;
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` ADD COLUMN `tracking_type` VARCHAR(30) COMMENT '트래킹 유형' AFTER `tracking_code_format`;
	
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `tracking_code_format`='EM*********KR' WHERE `gno` IN (101, 102);
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `tracking_code_format`='ET*********KR' WHERE `gno` IN (103, 104);
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `tracking_type`='EMS Track' WHERE `gno` IN (101, 102, 103, 104);
	
	UPDATE `{$installer->getTable('koreapost/goods')}` 
	SET `tracking_code_format`='Not Available', `tracking_type`='Not Available'  
	WHERE `gno` IN (105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115);
	
	UPDATE `{$installer->getTable('koreapost/goods')}` 
	SET `tracking_code_format`='RR*********KR', `tracking_type`='Insternational Registration'  
	WHERE `gno`=116;
	
");

$installer->endSetup();