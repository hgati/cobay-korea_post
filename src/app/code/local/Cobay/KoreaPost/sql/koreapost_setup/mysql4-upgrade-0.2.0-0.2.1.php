<?php 
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE `{$installer->getTable('koreapost/country')}` 
		ADD COLUMN `rr_full_tracking` TINYINT COMMENT '완전한 RR등기추적여부' AFTER `country_ko`;
");

$installer->endSetup();