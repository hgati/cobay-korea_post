<?php 
$installer = $this;
$installer->startSetup();

$installer->run("
	UPDATE `{$installer->getTable('koreapost/goods')}` 
	SET 
		`gnm`='항공 서장우편', `gnm_en`='Air Mail', `s_deli_time`=14, 
		`e_deli_time`=20, `cost_ment`='Cheapest', `max_weight`=2 
	WHERE `gno`=107;

	UPDATE `{$installer->getTable('koreapost/goods')}` 
	SET `gnm`='선편 서장우편', `gnm_en`='Surface Mail' 
	WHERE `gno`=108;	
");

$installer->endSetup();