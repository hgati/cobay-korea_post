<?php 
$installer = $this;
$installer->startSetup();

/* SAL 소포 무게구성표 잘못된 데이터 수정 */
$installer->run("

UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=0.0000, `ewei`=1.0000 WHERE `gno`=113 AND `swei`=0.0000 AND `ewei`=0.0010;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=1.0001, `ewei`=2.0000 WHERE `gno`=113 AND `swei`=0.0011 AND `ewei`=0.0020;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=2.0001, `ewei`=3.0000 WHERE `gno`=113 AND `swei`=0.0021 AND `ewei`=0.0030;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=3.0001, `ewei`=4.0000 WHERE `gno`=113 AND `swei`=0.0031 AND `ewei`=0.0040;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=4.0001, `ewei`=5.0000 WHERE `gno`=113 AND `swei`=0.0041 AND `ewei`=0.0050;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=5.0001, `ewei`=6.0000 WHERE `gno`=113 AND `swei`=0.0051 AND `ewei`=0.0060;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=6.0001, `ewei`=7.0000 WHERE `gno`=113 AND `swei`=0.0061 AND `ewei`=0.0070;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=7.0001, `ewei`=8.0000 WHERE `gno`=113 AND `swei`=0.0071 AND `ewei`=0.0080;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=8.0001, `ewei`=9.0000 WHERE `gno`=113 AND `swei`=0.0081 AND `ewei`=0.0090;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=9.0001, `ewei`=10.0000 WHERE `gno`=113 AND `swei`=0.0091 AND `ewei`=0.0100;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=10.0001, `ewei`=11.0000 WHERE `gno`=113 AND `swei`=0.0101 AND `ewei`=0.0110;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=11.0001, `ewei`=12.0000 WHERE `gno`=113 AND `swei`=0.0111 AND `ewei`=0.0120;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=12.0001, `ewei`=13.0000 WHERE `gno`=113 AND `swei`=0.0121 AND `ewei`=0.0130;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=13.0001, `ewei`=14.0000 WHERE `gno`=113 AND `swei`=0.0131 AND `ewei`=0.0140;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=14.0001, `ewei`=15.0000 WHERE `gno`=113 AND `swei`=0.0141 AND `ewei`=0.0150;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=15.0001, `ewei`=16.0000 WHERE `gno`=113 AND `swei`=0.0151 AND `ewei`=0.0160;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=16.0001, `ewei`=17.0000 WHERE `gno`=113 AND `swei`=0.0161 AND `ewei`=0.0170;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=17.0001, `ewei`=18.0000 WHERE `gno`=113 AND `swei`=0.0171 AND `ewei`=0.0180;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=18.0001, `ewei`=19.0000 WHERE `gno`=113 AND `swei`=0.0181 AND `ewei`=0.0190;
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=19.0001, `ewei`=20.0000 WHERE `gno`=113 AND `swei`=0.0191 AND `ewei`=0.0120;


UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=0.0000, `ewei`=1.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0000 AND `ewei`=0.0010;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=1.0001, `ewei`=2.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0011 AND `ewei`=0.0020;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=2.0001, `ewei`=3.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0021 AND `ewei`=0.0030;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=3.0001, `ewei`=4.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0031 AND `ewei`=0.0040;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=4.0001, `ewei`=5.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0041 AND `ewei`=0.0050;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=5.0001, `ewei`=6.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0051 AND `ewei`=0.0060;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=6.0001, `ewei`=7.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0061 AND `ewei`=0.0070;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=7.0001, `ewei`=8.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0071 AND `ewei`=0.0080;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=8.0001, `ewei`=9.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0081 AND `ewei`=0.0090;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=9.0001, `ewei`=10.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0091 AND `ewei`=0.0100;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=10.0001, `ewei`=11.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0101 AND `ewei`=0.0110;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=11.0001, `ewei`=12.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0111 AND `ewei`=0.0120;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=12.0001, `ewei`=13.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0121 AND `ewei`=0.0130;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=13.0001, `ewei`=14.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0131 AND `ewei`=0.0140;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=14.0001, `ewei`=15.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0141 AND `ewei`=0.0150;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=15.0001, `ewei`=16.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0151 AND `ewei`=0.0160;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=16.0001, `ewei`=17.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0161 AND `ewei`=0.0170;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=17.0001, `ewei`=18.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0171 AND `ewei`=0.0180;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=18.0001, `ewei`=19.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0181 AND `ewei`=0.0190;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=19.0001, `ewei`=20.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0191 AND `ewei`=0.0120;


");

$installer->endSetup();