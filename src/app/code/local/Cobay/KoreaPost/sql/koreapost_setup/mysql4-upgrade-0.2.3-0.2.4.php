<?php 
$installer = $this;
$installer->startSetup();

/* SAL 소포 무게구성표 잘못된 데이터 수정 */
$installer->run("
UPDATE `{$installer->getTable('koreapost/GoodsWeight')}` SET `swei`=19.0001, `ewei`=20.0000 WHERE `gno`=113 AND `swei`=0.0191 AND `ewei`=0.0200;
UPDATE `{$installer->getTable('koreapost/rate')}` SET `swei`=19.0001, `ewei`=20.0000 WHERE `gno`=113 AND `region`=400 AND `swei`=0.0191 AND `ewei`=0.0200;
");

$installer->endSetup();