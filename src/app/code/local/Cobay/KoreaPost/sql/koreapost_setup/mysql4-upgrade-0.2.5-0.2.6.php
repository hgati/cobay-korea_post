<?php 
$installer = $this;
$installer->startSetup();

$installer->run("
INSERT INTO `{$installer->getTable('koreapost/goods')}`(`gno`, `gnm`, `gnm_en`, `gnm_en_sub`, `s_deli_time`, `e_deli_time`, `cost_ment`, `max_weight`, `tracking_code_format`, `tracking_type`, `carrier_code`) 
VALUES (117, 'K-Packet', 'K-Packet', 'Air', 14, 20, 'Cheap', 2, 'Not Available', 'Not Available', 'KPKT');
");

$installer->endSetup();