<?php 
$installer = $this;
$installer->startSetup();

/* EMS 서류 (101) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=101;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=101;
");

/* EMS 서류 (101) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (101, 0.0000, 0.3000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=0.3000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (101, 0.3001, 0.5000) ON DUPLICATE KEY UPDATE swei=0.3001, ewei=0.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (101, 0.5001, 0.7500) ON DUPLICATE KEY UPDATE swei=0.5001, ewei=0.7500;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (101, 0.7501, 1.0000) ON DUPLICATE KEY UPDATE swei=0.7501, ewei=1.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (101, 1.0001, 1.2500) ON DUPLICATE KEY UPDATE swei=1.0001, ewei=1.2500;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (101, 1.2501, 1.5000) ON DUPLICATE KEY UPDATE swei=1.2501, ewei=1.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (101, 1.5001, 1.7500) ON DUPLICATE KEY UPDATE swei=1.5001, ewei=1.7500;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (101, 1.7501, 2.0000) ON DUPLICATE KEY UPDATE swei=1.7501, ewei=2.0000;
");

/* EMS 서류 (101) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 201, 0.0000, 0.3000, 14100) ON DUPLICATE KEY UPDATE fee=14100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 202, 0.0000, 0.3000, 13000) ON DUPLICATE KEY UPDATE fee=13000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 203, 0.0000, 0.3000, 16800) ON DUPLICATE KEY UPDATE fee=16800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 204, 0.0000, 0.3000, 17500) ON DUPLICATE KEY UPDATE fee=17500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 301, 0.0000, 0.3000, 10900) ON DUPLICATE KEY UPDATE fee=10900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 302, 0.0000, 0.3000, 10900) ON DUPLICATE KEY UPDATE fee=10900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 303, 0.0000, 0.3000, 10900) ON DUPLICATE KEY UPDATE fee=10900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 304, 0.0000, 0.3000, 16500) ON DUPLICATE KEY UPDATE fee=16500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 305, 0.0000, 0.3000, 19100) ON DUPLICATE KEY UPDATE fee=19100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 306, 0.0000, 0.3000, 16700) ON DUPLICATE KEY UPDATE fee=16700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 201, 0.3001, 0.5000, 16200) ON DUPLICATE KEY UPDATE fee=16200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 202, 0.3001, 0.5000, 15100) ON DUPLICATE KEY UPDATE fee=15100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 203, 0.3001, 0.5000, 19000) ON DUPLICATE KEY UPDATE fee=19000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 204, 0.3001, 0.5000, 21100) ON DUPLICATE KEY UPDATE fee=21100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 301, 0.3001, 0.5000, 12600) ON DUPLICATE KEY UPDATE fee=12600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 302, 0.3001, 0.5000, 12600) ON DUPLICATE KEY UPDATE fee=12600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 303, 0.3001, 0.5000, 12600) ON DUPLICATE KEY UPDATE fee=12600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 304, 0.3001, 0.5000, 18800) ON DUPLICATE KEY UPDATE fee=18800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 305, 0.3001, 0.5000, 21900) ON DUPLICATE KEY UPDATE fee=21900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 306, 0.3001, 0.5000, 18900) ON DUPLICATE KEY UPDATE fee=18900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 201, 0.5001, 0.7500, 17700) ON DUPLICATE KEY UPDATE fee=17700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 202, 0.5001, 0.7500, 17500) ON DUPLICATE KEY UPDATE fee=17500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 203, 0.5001, 0.7500, 22800) ON DUPLICATE KEY UPDATE fee=22800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 204, 0.5001, 0.7500, 24700) ON DUPLICATE KEY UPDATE fee=24700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 301, 0.5001, 0.7500, 13800) ON DUPLICATE KEY UPDATE fee=13800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 302, 0.5001, 0.7500, 13800) ON DUPLICATE KEY UPDATE fee=13800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 303, 0.5001, 0.7500, 13800) ON DUPLICATE KEY UPDATE fee=13800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 304, 0.5001, 0.7500, 22600) ON DUPLICATE KEY UPDATE fee=22600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 305, 0.5001, 0.7500, 23700) ON DUPLICATE KEY UPDATE fee=23700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 306, 0.5001, 0.7500, 22600) ON DUPLICATE KEY UPDATE fee=22600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 201, 0.7501, 1.0000, 19200) ON DUPLICATE KEY UPDATE fee=19200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 202, 0.7501, 1.0000, 20000) ON DUPLICATE KEY UPDATE fee=20000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 203, 0.7501, 1.0000, 26600) ON DUPLICATE KEY UPDATE fee=26600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 204, 0.7501, 1.0000, 28300) ON DUPLICATE KEY UPDATE fee=28300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 301, 0.7501, 1.0000, 15100) ON DUPLICATE KEY UPDATE fee=15100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 302, 0.7501, 1.0000, 15100) ON DUPLICATE KEY UPDATE fee=15100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 303, 0.7501, 1.0000, 15100) ON DUPLICATE KEY UPDATE fee=15100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 304, 0.7501, 1.0000, 26400) ON DUPLICATE KEY UPDATE fee=26400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 305, 0.7501, 1.0000, 25600) ON DUPLICATE KEY UPDATE fee=25600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 306, 0.7501, 1.0000, 26400) ON DUPLICATE KEY UPDATE fee=26400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 201, 1.0001, 1.2500, 21000) ON DUPLICATE KEY UPDATE fee=21000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 202, 1.0001, 1.2500, 23200) ON DUPLICATE KEY UPDATE fee=23200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 203, 1.0001, 1.2500, 31600) ON DUPLICATE KEY UPDATE fee=31600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 204, 1.0001, 1.2500, 33600) ON DUPLICATE KEY UPDATE fee=33600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 301, 1.0001, 1.2500, 16700) ON DUPLICATE KEY UPDATE fee=16700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 302, 1.0001, 1.2500, 16700) ON DUPLICATE KEY UPDATE fee=16700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 303, 1.0001, 1.2500, 16700) ON DUPLICATE KEY UPDATE fee=16700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 304, 1.0001, 1.2500, 30500) ON DUPLICATE KEY UPDATE fee=30500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 305, 1.0001, 1.2500, 30400) ON DUPLICATE KEY UPDATE fee=30400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 306, 1.0001, 1.2500, 31400) ON DUPLICATE KEY UPDATE fee=31400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 201, 1.2501, 1.5000, 22800) ON DUPLICATE KEY UPDATE fee=22800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 202, 1.2501, 1.5000, 26500) ON DUPLICATE KEY UPDATE fee=26500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 203, 1.2501, 1.5000, 36700) ON DUPLICATE KEY UPDATE fee=36700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 204, 1.2501, 1.5000, 39000) ON DUPLICATE KEY UPDATE fee=39000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 301, 1.2501, 1.5000, 18300) ON DUPLICATE KEY UPDATE fee=18300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 302, 1.2501, 1.5000, 18300) ON DUPLICATE KEY UPDATE fee=18300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 303, 1.2501, 1.5000, 18300) ON DUPLICATE KEY UPDATE fee=18300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 304, 1.2501, 1.5000, 35000) ON DUPLICATE KEY UPDATE fee=35000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 305, 1.2501, 1.5000, 35200) ON DUPLICATE KEY UPDATE fee=35200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 306, 1.2501, 1.5000, 36500) ON DUPLICATE KEY UPDATE fee=36500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 201, 1.5001, 1.7500, 24000) ON DUPLICATE KEY UPDATE fee=24000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 202, 1.5001, 1.7500, 29000) ON DUPLICATE KEY UPDATE fee=29000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 203, 1.5001, 1.7500, 42300) ON DUPLICATE KEY UPDATE fee=42300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 204, 1.5001, 1.7500, 44200) ON DUPLICATE KEY UPDATE fee=44200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 301, 1.5001, 1.7500, 19800) ON DUPLICATE KEY UPDATE fee=19800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 302, 1.5001, 1.7500, 19800) ON DUPLICATE KEY UPDATE fee=19800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 303, 1.5001, 1.7500, 19800) ON DUPLICATE KEY UPDATE fee=19800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 304, 1.5001, 1.7500, 39000) ON DUPLICATE KEY UPDATE fee=39000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 305, 1.5001, 1.7500, 38800) ON DUPLICATE KEY UPDATE fee=38800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 306, 1.5001, 1.7500, 42000) ON DUPLICATE KEY UPDATE fee=42000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 201, 1.7501, 2.0000, 25500) ON DUPLICATE KEY UPDATE fee=25500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 202, 1.7501, 2.0000, 32000) ON DUPLICATE KEY UPDATE fee=32000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 203, 1.7501, 2.0000, 47900) ON DUPLICATE KEY UPDATE fee=47900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 204, 1.7501, 2.0000, 49400) ON DUPLICATE KEY UPDATE fee=49400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 301, 1.7501, 2.0000, 21400) ON DUPLICATE KEY UPDATE fee=21400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 302, 1.7501, 2.0000, 21400) ON DUPLICATE KEY UPDATE fee=21400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 303, 1.7501, 2.0000, 21400) ON DUPLICATE KEY UPDATE fee=21400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 304, 1.7501, 2.0000, 43000) ON DUPLICATE KEY UPDATE fee=43000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 305, 1.7501, 2.0000, 42400) ON DUPLICATE KEY UPDATE fee=42400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (101, 306, 1.7501, 2.0000, 47500) ON DUPLICATE KEY UPDATE fee=47500;
");


$installer->endSetup();