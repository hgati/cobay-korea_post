<?php 
$installer = $this;
$installer->startSetup();

/* EMS 프리미엄 서류 (103) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=103;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=103;
");

/* EMS 프리미엄 서류 (103) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (103, 0.0000, 0.5000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=0.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (103, 0.5001, 1.0000) ON DUPLICATE KEY UPDATE swei=0.5001, ewei=1.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (103, 1.0001, 1.5000) ON DUPLICATE KEY UPDATE swei=1.0001, ewei=1.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (103, 1.5001, 2.0000) ON DUPLICATE KEY UPDATE swei=1.5001, ewei=2.0000;
");

/* EMS 프리미엄 서류 (103) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 201, 0.0000, 0.5000, 13700) ON DUPLICATE KEY UPDATE fee=13700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 202, 0.0000, 0.5000, 14700) ON DUPLICATE KEY UPDATE fee=14700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 203, 0.0000, 0.5000, 18900) ON DUPLICATE KEY UPDATE fee=18900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 204, 0.0000, 0.5000, 24700) ON DUPLICATE KEY UPDATE fee=24700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 205, 0.0000, 0.5000, 26300) ON DUPLICATE KEY UPDATE fee=26300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 201, 0.5001, 1.0000, 17300) ON DUPLICATE KEY UPDATE fee=17300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 202, 0.5001, 1.0000, 22100) ON DUPLICATE KEY UPDATE fee=22100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 203, 0.5001, 1.0000, 26300) ON DUPLICATE KEY UPDATE fee=26300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 204, 0.5001, 1.0000, 31500) ON DUPLICATE KEY UPDATE fee=31500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 205, 0.5001, 1.0000, 34700) ON DUPLICATE KEY UPDATE fee=34700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 201, 1.0001, 1.5000, 21000) ON DUPLICATE KEY UPDATE fee=21000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 202, 1.0001, 1.5000, 30500) ON DUPLICATE KEY UPDATE fee=30500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 203, 1.0001, 1.5000, 37800) ON DUPLICATE KEY UPDATE fee=37800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 204, 1.0001, 1.5000, 41000) ON DUPLICATE KEY UPDATE fee=41000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 205, 1.0001, 1.5000, 44100) ON DUPLICATE KEY UPDATE fee=44100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 201, 1.5001, 2.0000, 25200) ON DUPLICATE KEY UPDATE fee=25200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 202, 1.5001, 2.0000, 37800) ON DUPLICATE KEY UPDATE fee=37800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 203, 1.5001, 2.0000, 47300) ON DUPLICATE KEY UPDATE fee=47300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 204, 1.5001, 2.0000, 50400) ON DUPLICATE KEY UPDATE fee=50400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (103, 205, 1.5001, 2.0000, 52500) ON DUPLICATE KEY UPDATE fee=52500;
");

$installer->endSetup();