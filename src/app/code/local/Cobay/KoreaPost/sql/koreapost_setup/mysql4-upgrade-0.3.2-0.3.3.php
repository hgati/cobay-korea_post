<?php 
$installer = $this;
$installer->startSetup();

/* 국제(선편)소포 (114) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=114;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=114;
");

/* 국제(선편)소포 (114) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 0.0000, 2.0000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=2.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 2.0001, 4.0000) ON DUPLICATE KEY UPDATE swei=2.0001, ewei=4.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 4.0001, 6.0000) ON DUPLICATE KEY UPDATE swei=4.0001, ewei=6.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 6.0001, 8.0000) ON DUPLICATE KEY UPDATE swei=6.0001, ewei=8.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 8.0001, 10.0000) ON DUPLICATE KEY UPDATE swei=8.0001, ewei=10.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 10.0001, 12.0000) ON DUPLICATE KEY UPDATE swei=10.0001, ewei=12.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 12.0001, 14.0000) ON DUPLICATE KEY UPDATE swei=12.0001, ewei=14.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 14.0001, 16.0000) ON DUPLICATE KEY UPDATE swei=14.0001, ewei=16.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 16.0001, 18.0000) ON DUPLICATE KEY UPDATE swei=16.0001, ewei=18.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (114, 18.0001, 20.0000) ON DUPLICATE KEY UPDATE swei=18.0001, ewei=20.0000;
");

/* 국제(선편)소포 (114) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 0.0000, 2.0000, 11100) ON DUPLICATE KEY UPDATE fee=11100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 0.0000, 2.0000, 12200) ON DUPLICATE KEY UPDATE fee=12200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 0.0000, 2.0000, 13300) ON DUPLICATE KEY UPDATE fee=13300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 0.0000, 2.0000, 14500) ON DUPLICATE KEY UPDATE fee=14500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 2.0001, 4.0000, 14500) ON DUPLICATE KEY UPDATE fee=14500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 2.0001, 4.0000, 15600) ON DUPLICATE KEY UPDATE fee=15600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 2.0001, 4.0000, 17800) ON DUPLICATE KEY UPDATE fee=17800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 2.0001, 4.0000, 20000) ON DUPLICATE KEY UPDATE fee=20000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 4.0001, 6.0000, 17800) ON DUPLICATE KEY UPDATE fee=17800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 4.0001, 6.0000, 18900) ON DUPLICATE KEY UPDATE fee=18900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 4.0001, 6.0000, 22300) ON DUPLICATE KEY UPDATE fee=22300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 4.0001, 6.0000, 25700) ON DUPLICATE KEY UPDATE fee=25700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 6.0001, 8.0000, 21200) ON DUPLICATE KEY UPDATE fee=21200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 6.0001, 8.0000, 22300) ON DUPLICATE KEY UPDATE fee=22300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 6.0001, 8.0000, 26700) ON DUPLICATE KEY UPDATE fee=26700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 6.0001, 8.0000, 31300) ON DUPLICATE KEY UPDATE fee=31300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 8.0001, 10.0000, 24600) ON DUPLICATE KEY UPDATE fee=24600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 8.0001, 10.0000, 25700) ON DUPLICATE KEY UPDATE fee=25700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 8.0001, 10.0000, 31300) ON DUPLICATE KEY UPDATE fee=31300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 8.0001, 10.0000, 36800) ON DUPLICATE KEY UPDATE fee=36800;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 10.0001, 12.0000, 27900) ON DUPLICATE KEY UPDATE fee=27900;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 10.0001, 12.0000, 29000) ON DUPLICATE KEY UPDATE fee=29000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 10.0001, 12.0000, 35700) ON DUPLICATE KEY UPDATE fee=35700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 10.0001, 12.0000, 42500) ON DUPLICATE KEY UPDATE fee=42500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 12.0001, 14.0000, 31300) ON DUPLICATE KEY UPDATE fee=31300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 12.0001, 14.0000, 32400) ON DUPLICATE KEY UPDATE fee=32400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 12.0001, 14.0000, 40200) ON DUPLICATE KEY UPDATE fee=40200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 12.0001, 14.0000, 48100) ON DUPLICATE KEY UPDATE fee=48100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 14.0001, 16.0000, 34600) ON DUPLICATE KEY UPDATE fee=34600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 14.0001, 16.0000, 35700) ON DUPLICATE KEY UPDATE fee=35700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 14.0001, 16.0000, 44700) ON DUPLICATE KEY UPDATE fee=44700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 14.0001, 16.0000, 53600) ON DUPLICATE KEY UPDATE fee=53600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 16.0001, 18.0000, 38000) ON DUPLICATE KEY UPDATE fee=38000;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 16.0001, 18.0000, 39100) ON DUPLICATE KEY UPDATE fee=39100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 16.0001, 18.0000, 49200) ON DUPLICATE KEY UPDATE fee=49200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 16.0001, 18.0000, 59300) ON DUPLICATE KEY UPDATE fee=59300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 201, 18.0001, 20.0000, 41400) ON DUPLICATE KEY UPDATE fee=41400;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 202, 18.0001, 20.0000, 42500) ON DUPLICATE KEY UPDATE fee=42500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 203, 18.0001, 20.0000, 53600) ON DUPLICATE KEY UPDATE fee=53600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (114, 204, 18.0001, 20.0000, 64800) ON DUPLICATE KEY UPDATE fee=64800;
");


$installer->endSetup();