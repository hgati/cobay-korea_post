<?php 
$installer = $this;
$installer->startSetup();

/* 항공 소형포장물 (105) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=105;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=105;
");

/* 항공 소형포장물 (105) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 0.0000, 100.0000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=100.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 100.0001, 250.0000) ON DUPLICATE KEY UPDATE swei=100.0001, ewei=250.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 250.0001, 500.0000) ON DUPLICATE KEY UPDATE swei=250.0001, ewei=500.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 500.0001, 1000.0000) ON DUPLICATE KEY UPDATE swei=500.0001, ewei=1000.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 1000.0001, 1500.0000) ON DUPLICATE KEY UPDATE swei=1000.0001, ewei=1500.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 1500.0001, 2000.0000) ON DUPLICATE KEY UPDATE swei=1500.0001, ewei=2000.0000;
");

/* 항공 소형포장물 (105) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 0.0000, 100.0000, 1170) ON DUPLICATE KEY UPDATE fee=1170;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 0.0000, 100.0000, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 0.0000, 100.0000, 1880) ON DUPLICATE KEY UPDATE fee=1880;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 0.0000, 100.0000, 2120) ON DUPLICATE KEY UPDATE fee=2120;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 100.0001, 250.0000, 2120) ON DUPLICATE KEY UPDATE fee=2120;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 100.0001, 250.0000, 2950) ON DUPLICATE KEY UPDATE fee=2950;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 100.0001, 250.0000, 3770) ON DUPLICATE KEY UPDATE fee=3770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 100.0001, 250.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 250.0001, 500.0000, 2950) ON DUPLICATE KEY UPDATE fee=2950;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 250.0001, 500.0000, 4720) ON DUPLICATE KEY UPDATE fee=4720;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 250.0001, 500.0000, 6500) ON DUPLICATE KEY UPDATE fee=6500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 250.0001, 500.0000, 6860) ON DUPLICATE KEY UPDATE fee=6860;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 500.0001, 1000.0000, 5320) ON DUPLICATE KEY UPDATE fee=5320;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 500.0001, 1000.0000, 7100) ON DUPLICATE KEY UPDATE fee=7100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 500.0001, 1000.0000, 10650) ON DUPLICATE KEY UPDATE fee=10650;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 500.0001, 1000.0000, 12770) ON DUPLICATE KEY UPDATE fee=12770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 1000.0001, 1500.0000, 7100) ON DUPLICATE KEY UPDATE fee=7100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 1000.0001, 1500.0000, 10650) ON DUPLICATE KEY UPDATE fee=10650;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 1000.0001, 1500.0000, 15970) ON DUPLICATE KEY UPDATE fee=15970;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 1000.0001, 1500.0000, 18330) ON DUPLICATE KEY UPDATE fee=18330;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 1500.0001, 2000.0000, 8270) ON DUPLICATE KEY UPDATE fee=8270;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 1500.0001, 2000.0000, 14200) ON DUPLICATE KEY UPDATE fee=14200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 1500.0001, 2000.0000, 21300) ON DUPLICATE KEY UPDATE fee=21300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 1500.0001, 2000.0000, 23670) ON DUPLICATE KEY UPDATE fee=23670;
");

$installer->endSetup();