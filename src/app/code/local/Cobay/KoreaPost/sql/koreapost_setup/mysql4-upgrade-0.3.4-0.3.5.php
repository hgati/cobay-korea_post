<?php 
$installer = $this;
$installer->startSetup();

/* 선편 소형포장물 (106) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=106;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=106;
");

/* 선편 소형포장물 (106) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 0.0000, 100.0000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=100.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 100.0001, 250.0000) ON DUPLICATE KEY UPDATE swei=100.0001, ewei=250.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 250.0001, 500.0000) ON DUPLICATE KEY UPDATE swei=250.0001, ewei=500.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 500.0001, 1000.0000) ON DUPLICATE KEY UPDATE swei=500.0001, ewei=1000.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 1000.0001, 2000.0000) ON DUPLICATE KEY UPDATE swei=1000.0001, ewei=2000.0000;
");

/* 선편 소형포장물 (106) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 0.0000, 100.0000, 930) ON DUPLICATE KEY UPDATE fee=930;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 0.0000, 100.0000, 930) ON DUPLICATE KEY UPDATE fee=930;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 0.0000, 100.0000, 930) ON DUPLICATE KEY UPDATE fee=930;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 0.0000, 100.0000, 930) ON DUPLICATE KEY UPDATE fee=930;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 100.0001, 250.0000, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 100.0001, 250.0000, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 100.0001, 250.0000, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 100.0001, 250.0000, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 250.0001, 500.0000, 2600) ON DUPLICATE KEY UPDATE fee=2600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 250.0001, 500.0000, 2600) ON DUPLICATE KEY UPDATE fee=2600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 250.0001, 500.0000, 2600) ON DUPLICATE KEY UPDATE fee=2600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 250.0001, 500.0000, 2600) ON DUPLICATE KEY UPDATE fee=2600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 500.0001, 1000.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 500.0001, 1000.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 500.0001, 1000.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 500.0001, 1000.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 1000.0001, 2000.0000, 5910) ON DUPLICATE KEY UPDATE fee=5910;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 1000.0001, 2000.0000, 5910) ON DUPLICATE KEY UPDATE fee=5910;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 1000.0001, 2000.0000, 5910) ON DUPLICATE KEY UPDATE fee=5910;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 1000.0001, 2000.0000, 5910) ON DUPLICATE KEY UPDATE fee=5910;
");


$installer->endSetup();