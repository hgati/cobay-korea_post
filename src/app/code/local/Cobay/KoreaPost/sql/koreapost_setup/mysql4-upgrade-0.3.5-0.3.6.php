<?php 
$installer = $this;
$installer->startSetup();

/* K-Packet (117) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=117;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=117;
");

/* K-Packet (117) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 0.0000, 100.0000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=100.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 100.0001, 250.0000) ON DUPLICATE KEY UPDATE swei=100.0001, ewei=250.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 250.0001, 500.0000) ON DUPLICATE KEY UPDATE swei=250.0001, ewei=500.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 500.0001, 1000.0000) ON DUPLICATE KEY UPDATE swei=500.0001, ewei=1000.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 1000.0001, 1500.0000) ON DUPLICATE KEY UPDATE swei=1000.0001, ewei=1500.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 1500.0001, 2000.0000) ON DUPLICATE KEY UPDATE swei=1500.0001, ewei=2000.0000;
");

/* K-Packet (117) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 0.0000, 100.0000, 4170) ON DUPLICATE KEY UPDATE fee=4170;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 0.0000, 100.0000, 4680) ON DUPLICATE KEY UPDATE fee=4680;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 0.0000, 100.0000, 4870) ON DUPLICATE KEY UPDATE fee=4870;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 0.0000, 100.0000, 5070) ON DUPLICATE KEY UPDATE fee=5070;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 100.0001, 250.0000, 5610) ON DUPLICATE KEY UPDATE fee=5610;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 100.0001, 250.0000, 6310) ON DUPLICATE KEY UPDATE fee=6310;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 100.0001, 250.0000, 6990) ON DUPLICATE KEY UPDATE fee=6990;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 100.0001, 250.0000, 7700) ON DUPLICATE KEY UPDATE fee=7700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 250.0001, 500.0000, 8560) ON DUPLICATE KEY UPDATE fee=8560;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 250.0001, 500.0000, 9160) ON DUPLICATE KEY UPDATE fee=9160;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 250.0001, 500.0000, 10810) ON DUPLICATE KEY UPDATE fee=10810;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 250.0001, 500.0000, 11150) ON DUPLICATE KEY UPDATE fee=11150;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 500.0001, 1000.0000, 12920) ON DUPLICATE KEY UPDATE fee=12920;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 500.0001, 1000.0000, 14460) ON DUPLICATE KEY UPDATE fee=14460;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 500.0001, 1000.0000, 16580) ON DUPLICATE KEY UPDATE fee=16580;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 500.0001, 1000.0000, 18120) ON DUPLICATE KEY UPDATE fee=18120;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 1000.0001, 1500.0000, 16810) ON DUPLICATE KEY UPDATE fee=16810;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 1000.0001, 1500.0000, 20470) ON DUPLICATE KEY UPDATE fee=20470;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 1000.0001, 1500.0000, 24880) ON DUPLICATE KEY UPDATE fee=24880;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 1000.0001, 1500.0000, 27320) ON DUPLICATE KEY UPDATE fee=27320;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 1500.0001, 2000.0000, 19140) ON DUPLICATE KEY UPDATE fee=19140;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 1500.0001, 2000.0000, 26470) ON DUPLICATE KEY UPDATE fee=26470;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 1500.0001, 2000.0000, 32320) ON DUPLICATE KEY UPDATE fee=32320;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 1500.0001, 2000.0000, 35570) ON DUPLICATE KEY UPDATE fee=35570;
");

$installer->endSetup();