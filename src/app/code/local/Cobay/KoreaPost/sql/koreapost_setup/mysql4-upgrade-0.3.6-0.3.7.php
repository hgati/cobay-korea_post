<?php 
$installer = $this;
$installer->startSetup();

/* SAL 소포 폐지되어 삭제함 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/goods')}` WHERE gno=113;
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=113;
DELETE FROM `{$installer->getTable('koreapost/GoodsDeliveryTime')}` WHERE gno=113;
");

$installer->endSetup();