<?php 
$installer = $this;
$installer->startSetup();

/* EMS 서류 (101) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=101;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=101;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 301);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 302);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 303);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 304);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 305);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (101, 306);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=101 AND a.gno=b.gno AND b.region=c.ems_region;
");

/* EMS 비서류 (102) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=102;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=102;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 301);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 302);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 303);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 304);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 305);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (102, 306);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=102 AND a.gno=b.gno AND b.region=c.ems_region;
");

/* EMS 프리미엄 서류 (103) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=103;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=103;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (103, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (103, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (103, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (103, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (103, 205);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=103 AND a.gno=b.gno AND b.region=c.ems_pri_region;
");

/* EMS 프리미엄 비서류 (104) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=104;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=104;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (104, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (104, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (104, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (104, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (104, 205);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=104 AND a.gno=b.gno AND b.region=c.ems_pri_region;
");

/* K-Packet (117) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=117;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=117;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=117 AND a.gno=b.gno AND b.region=c.air_trade_region;
");

/* 항공 소형포장물 (105) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=105;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=105;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=105 AND a.gno=b.gno AND b.region=c.air_trade_region;
");

/* 선편 소형포장물 (106) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=106;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=106;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=106 AND a.gno=b.gno AND b.region=c.air_trade_region;
");

/* 국제(항공)소포 (112) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=112;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=112;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=112 AND a.gno=b.gno AND b.region=c.ems_region;
");

/* 국제(선편)소포 (114) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=114;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=114;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=114 AND a.gno=b.gno AND b.region=c.air_trade_region;
");

$installer->endSetup();