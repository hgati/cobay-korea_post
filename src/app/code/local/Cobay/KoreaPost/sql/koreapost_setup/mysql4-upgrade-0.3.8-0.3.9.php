<?php 
$installer = $this;
$installer->startSetup();

/* K-Packet (117) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=117;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=117;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (117, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (117, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (117, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (117, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=117 AND a.gno=b.gno AND b.region=c.air_trade_region;
");

/* 항공 소형포장물 (105) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=105;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=105;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (105, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (105, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (105, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (105, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=105 AND a.gno=b.gno AND b.region=c.air_trade_region;
");

/* 선편 소형포장물 (106) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=106;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=106;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (106, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (106, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=106 AND a.gno=b.gno AND b.region=c.air_trade_region;
");

/* 국제(항공)소포 (112) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=112;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=112;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 301);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 302);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 303);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 304);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 305);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (112, 306);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=112 AND a.gno=b.gno AND b.region=c.ems_region;
");

/* 국제(선편)소포 (114) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=114;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=114;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (114, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (114, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (114, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (114, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=114 AND a.gno=b.gno AND b.region=c.air_trade_region;
");


$installer->endSetup();