<?php 
$installer = $this;
$installer->startSetup();

/* 선편 소형포장물 (106) - cobay_kpost_goods_region T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/GoodsRegionCountry')}` WHERE gno=106;
DELETE FROM `{$installer->getTable('koreapost/GoodsRegion')}` WHERE gno=106;
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (106, 201);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (106, 202);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (106, 203);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegion')}`(`gno`, `region`) VALUES (106, 204);
INSERT INTO `{$installer->getTable('koreapost/GoodsRegionCountry')}`(gno, region, iso3)
SELECT a.gno, b.region, c.iso3
FROM cobay_kpost_goods a, cobay_kpost_goods_region b, cobay_kpost_country_ c
WHERE a.gno=106 AND a.gno=b.gno AND b.region=c.air_trade_region;
");


$installer->endSetup();