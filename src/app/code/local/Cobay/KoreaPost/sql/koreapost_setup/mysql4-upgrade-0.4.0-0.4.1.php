<?php 
$installer = $this;
$installer->startSetup();

/* 항공 소형포장물 (105) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=105;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=105;
");

/* 항공 소형포장물 (105) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 0.0000, 0.1000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=0.1000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 0.1001, 0.2500) ON DUPLICATE KEY UPDATE swei=0.1001, ewei=0.2500;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 0.2501, 0.5000) ON DUPLICATE KEY UPDATE swei=0.2501, ewei=0.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 0.5001, 1.0000) ON DUPLICATE KEY UPDATE swei=0.5001, ewei=1.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 1.0001, 1.5000) ON DUPLICATE KEY UPDATE swei=1.0001, ewei=1.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (105, 1.5001, 2.0000) ON DUPLICATE KEY UPDATE swei=1.5001, ewei=2.0000;
");

/* 항공 소형포장물 (105) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 0.0000, 0.1000, 1170) ON DUPLICATE KEY UPDATE fee=1170;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 0.0000, 0.1000, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 0.0000, 0.1000, 1880) ON DUPLICATE KEY UPDATE fee=1880;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 0.0000, 0.1000, 2120) ON DUPLICATE KEY UPDATE fee=2120;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 0.1001, 0.2500, 2120) ON DUPLICATE KEY UPDATE fee=2120;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 0.1001, 0.2500, 2950) ON DUPLICATE KEY UPDATE fee=2950;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 0.1001, 0.2500, 3770) ON DUPLICATE KEY UPDATE fee=3770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 0.1001, 0.2500, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 0.2501, 0.5000, 2950) ON DUPLICATE KEY UPDATE fee=2950;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 0.2501, 0.5000, 4720) ON DUPLICATE KEY UPDATE fee=4720;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 0.2501, 0.5000, 6500) ON DUPLICATE KEY UPDATE fee=6500;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 0.2501, 0.5000, 6860) ON DUPLICATE KEY UPDATE fee=6860;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 0.5001, 1.0000, 5320) ON DUPLICATE KEY UPDATE fee=5320;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 0.5001, 1.0000, 7100) ON DUPLICATE KEY UPDATE fee=7100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 0.5001, 1.0000, 10650) ON DUPLICATE KEY UPDATE fee=10650;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 0.5001, 1.0000, 12770) ON DUPLICATE KEY UPDATE fee=12770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 1.0001, 1.5000, 7100) ON DUPLICATE KEY UPDATE fee=7100;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 1.0001, 1.5000, 10650) ON DUPLICATE KEY UPDATE fee=10650;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 1.0001, 1.5000, 15970) ON DUPLICATE KEY UPDATE fee=15970;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 1.0001, 1.5000, 18330) ON DUPLICATE KEY UPDATE fee=18330;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 201, 1.5001, 2.0000, 8270) ON DUPLICATE KEY UPDATE fee=8270;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 202, 1.5001, 2.0000, 14200) ON DUPLICATE KEY UPDATE fee=14200;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 203, 1.5001, 2.0000, 21300) ON DUPLICATE KEY UPDATE fee=21300;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (105, 204, 1.5001, 2.0000, 23670) ON DUPLICATE KEY UPDATE fee=23670;
");

/* 선편 소형포장물 (106) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=106;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=106;
");

/* 선편 소형포장물 (106) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 0.0000, 0.1000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=0.1000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 0.1001, 0.2500) ON DUPLICATE KEY UPDATE swei=0.1001, ewei=0.2500;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 0.2501, 0.5000) ON DUPLICATE KEY UPDATE swei=0.2501, ewei=0.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 0.5001, 1.0000) ON DUPLICATE KEY UPDATE swei=0.5001, ewei=1.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (106, 1.0001, 2.0000) ON DUPLICATE KEY UPDATE swei=1.0001, ewei=2.0000;
");

/* 선편 소형포장물 (106) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 0.0000, 0.1000, 930) ON DUPLICATE KEY UPDATE fee=930;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 0.0000, 0.1000, 930) ON DUPLICATE KEY UPDATE fee=930;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 0.0000, 0.1000, 930) ON DUPLICATE KEY UPDATE fee=930;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 0.0000, 0.1000, 930) ON DUPLICATE KEY UPDATE fee=930;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 0.1001, 0.2500, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 0.1001, 0.2500, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 0.1001, 0.2500, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 0.1001, 0.2500, 1770) ON DUPLICATE KEY UPDATE fee=1770;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 0.2501, 0.5000, 2600) ON DUPLICATE KEY UPDATE fee=2600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 0.2501, 0.5000, 2600) ON DUPLICATE KEY UPDATE fee=2600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 0.2501, 0.5000, 2600) ON DUPLICATE KEY UPDATE fee=2600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 0.2501, 0.5000, 2600) ON DUPLICATE KEY UPDATE fee=2600;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 0.5001, 1.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 0.5001, 1.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 0.5001, 1.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 0.5001, 1.0000, 4490) ON DUPLICATE KEY UPDATE fee=4490;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 201, 1.0001, 2.0000, 5910) ON DUPLICATE KEY UPDATE fee=5910;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 202, 1.0001, 2.0000, 5910) ON DUPLICATE KEY UPDATE fee=5910;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 203, 1.0001, 2.0000, 5910) ON DUPLICATE KEY UPDATE fee=5910;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (106, 204, 1.0001, 2.0000, 5910) ON DUPLICATE KEY UPDATE fee=5910;
");

/* K-Packet (117) - 테이블 초기화 */
$installer->run("
DELETE FROM `{$installer->getTable('koreapost/rate')}` WHERE gno=117;
DELETE FROM `{$installer->getTable('koreapost/GoodsWeight')}` WHERE gno=117;
");

/* K-Packet (117) - cobay_kpost_goods_wei T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 0.0000, 0.1000) ON DUPLICATE KEY UPDATE swei=0.0000, ewei=0.1000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 0.1001, 0.2500) ON DUPLICATE KEY UPDATE swei=0.1001, ewei=0.2500;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 0.2501, 0.5000) ON DUPLICATE KEY UPDATE swei=0.2501, ewei=0.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 0.5001, 1.0000) ON DUPLICATE KEY UPDATE swei=0.5001, ewei=1.0000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 1.0001, 1.5000) ON DUPLICATE KEY UPDATE swei=1.0001, ewei=1.5000;
INSERT INTO `{$installer->getTable('koreapost/GoodsWeight')}`(`gno`, `swei`, `ewei`) VALUES (117, 1.5001, 2.0000) ON DUPLICATE KEY UPDATE swei=1.5001, ewei=2.0000;
");

/* K-Packet (117) - cobay_kpost_rate T/B 있으면 업데이트 없으면 인서트 */
$installer->run("
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 0.0000, 0.1000, 4170) ON DUPLICATE KEY UPDATE fee=4170;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 0.0000, 0.1000, 4680) ON DUPLICATE KEY UPDATE fee=4680;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 0.0000, 0.1000, 4870) ON DUPLICATE KEY UPDATE fee=4870;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 0.0000, 0.1000, 5070) ON DUPLICATE KEY UPDATE fee=5070;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 0.1001, 0.2500, 5610) ON DUPLICATE KEY UPDATE fee=5610;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 0.1001, 0.2500, 6310) ON DUPLICATE KEY UPDATE fee=6310;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 0.1001, 0.2500, 6990) ON DUPLICATE KEY UPDATE fee=6990;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 0.1001, 0.2500, 7700) ON DUPLICATE KEY UPDATE fee=7700;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 0.2501, 0.5000, 8560) ON DUPLICATE KEY UPDATE fee=8560;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 0.2501, 0.5000, 9160) ON DUPLICATE KEY UPDATE fee=9160;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 0.2501, 0.5000, 10810) ON DUPLICATE KEY UPDATE fee=10810;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 0.2501, 0.5000, 11150) ON DUPLICATE KEY UPDATE fee=11150;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 0.5001, 1.0000, 12920) ON DUPLICATE KEY UPDATE fee=12920;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 0.5001, 1.0000, 14460) ON DUPLICATE KEY UPDATE fee=14460;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 0.5001, 1.0000, 16580) ON DUPLICATE KEY UPDATE fee=16580;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 0.5001, 1.0000, 18120) ON DUPLICATE KEY UPDATE fee=18120;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 1.0001, 1500.0000, 16810) ON DUPLICATE KEY UPDATE fee=16810;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 1.0001, 1500.0000, 20470) ON DUPLICATE KEY UPDATE fee=20470;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 1.0001, 1500.0000, 24880) ON DUPLICATE KEY UPDATE fee=24880;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 1.0001, 1500.0000, 27320) ON DUPLICATE KEY UPDATE fee=27320;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 201, 1.5001, 2.0000, 19140) ON DUPLICATE KEY UPDATE fee=19140;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 202, 1.5001, 2.0000, 26470) ON DUPLICATE KEY UPDATE fee=26470;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 203, 1.5001, 2.0000, 32320) ON DUPLICATE KEY UPDATE fee=32320;
INSERT INTO `{$installer->getTable('koreapost/rate')}`(`gno`, `region`, `swei`, `ewei`, `fee`) VALUES (117, 204, 1.5001, 2.0000, 35570) ON DUPLICATE KEY UPDATE fee=35570;
");

$installer->endSetup();