<?php 
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE `{$installer->getTable('koreapost/goods')}`
		ADD COLUMN `max_volumn` INT COMMENT '최대부피 = 길이+가로+높이(mm)' AFTER `max_weight`;
	ALTER TABLE `{$installer->getTable('koreapost/goods')}`
		ADD COLUMN `max_length` INT COMMENT '최대길이(mm)' AFTER `max_volumn`;

	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_volumn`=1800 WHERE `gno`=102;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_volumn`=1800 WHERE `gno`=104;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_volumn`=900 WHERE `gno`=105;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_volumn`=900 WHERE `gno`=107;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_volumn`=1800 WHERE `gno`=112;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_volumn`=900 WHERE `gno`=117;

	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_length`=600 WHERE `gno`=105;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_length`=600 WHERE `gno`=107;
	UPDATE `{$installer->getTable('koreapost/goods')}` SET `max_length`=600 WHERE `gno`=117;

");

$installer->endSetup();