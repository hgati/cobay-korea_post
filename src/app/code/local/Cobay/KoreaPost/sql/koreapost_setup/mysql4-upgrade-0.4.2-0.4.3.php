<?php 
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` DROP COLUMN `max_volumn`;
	ALTER TABLE `{$installer->getTable('koreapost/goods')}` DROP COLUMN `max_length`;
");

$installer->endSetup();