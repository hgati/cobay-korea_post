<?php 
$installer = $this;
$installer->startSetup();

$installer->run("
	DROP TABLE IF EXISTS `{$installer->getTable('koreapost/code')}`;
	
	CREATE TABLE `{$installer->getTable('koreapost/code')}`(
		`cod` SMALLINT UNSIGNED PRIMARY KEY NOT NULL COMMENT '코드',
		`cod_nm` VARCHAR(100) NOT NULL COMMENT '코드명칭',
		`p_cod` SMALLINT COMMENT '상위코드',
		`sort` TINYINT COMMENT '정렬순서',
		`desc` VARCHAR(255) COMMENT '설명'
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='공통코드';
	CREATE INDEX `idx_cobay_purchase_code_p_cod` ON `{$installer->getTable('koreapost/code')}` ( p_cod );
	
	INSERT INTO `{$installer->getTable('koreapost/code')}`
	(`cod`,`cod_nm`,`p_cod`,`sort`,`desc`)
	VALUES
	(1,'메일 서비스 프로바이더',NULL,1,NULL),
	(2,'hotmail.com',1,1,NULL),
	(3,'gmx.com',1,1,NULL),
	(4,'gmail.com',1,1,NULL),
	(100,'우체국 배송방식 구분',NULL,1,NULL),
	(101,'EMS(서류)',100,1,NULL),
	(102,'EMS(비서류)',100,2,NULL),
	(103,'EMS 프리미엄(서류)',100,3,NULL),
	(104,'EMS 프리미엄(비서류)',100,4,NULL),
	(105,'항공 소형포장물',100,5,NULL),
	(106,'선편 소형포장물',100,6,NULL),
	(107,'항공 서장',100,7,NULL),
	(108,'선편 서장',100,8,NULL),
	(109,'항공 엽서',100,9,NULL),
	(110,'선편 엽서',100,10,NULL),
	(111,'항공 서간',100,11,NULL),
	(112,'항공 소포',100,12,NULL),
	(113,'SAL 소포',100,13,NULL),
	(114,'선편 소포',100,14,NULL),
	(115,'취급주의및 규격외 소포',100,15,NULL),
	(200,'우체국 요금관리지역 구분',NULL,2,NULL),
	(201,'1지역',200,1,NULL),
	(202,'2지역',200,2,NULL),
	(203,'3지역',200,3,NULL),
	(204,'4지역',200,4,NULL),
	(205,'5지역',200,5,NULL),
	(301,'특정1지역',200,6,NULL),
	(302,'특정2지역',200,7,NULL),
	(303,'특정3지역',200,8,NULL),
	(304,'특정4지역',200,9,NULL),
	(305,'특정5지역',200,10,NULL),
	(306,'특정6지역',200,11,NULL),
	(400,'SAL관리지역',200,11,NULL),
	(401,'입/출고내역 비고구분',NULL,3,NULL),
	(402,'사용자 정의',401,1,NULL),
	(403,'시스템 최초  초기화 작업',401,2,NULL),
	(404,'상품수량 수정에 따른 시스템의 자동재고증감',401,3,NULL),
	(405,'상품등록에 따른 시스템의 자동재고입고처리',401,4,NULL);
");

$installer->endSetup();